<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Brand * </label>
    <div class="col-md-9">
        {!! Form::text('brand', $brand->name, ['class' => 'form-control m-b-5','placeholder'=>'Enter Name', 'data-parsley-required'=>'true', 'disabled']) !!}
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Feature * </label>
    <div class="col-md-9">
        {!! Form::select('product_feature_id', $features, null, ['class' => 'form-control m-b-5','placeholder'=>'Select', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('product_feature_id'))
            <span class="error-form">
            * {{ $errors->first('product_feature_id') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Value * </label>
    <div class="col-md-9">
        {!! Form::text('value', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Value', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('value'))
        <span class="error-form">
            * {{ $errors->first('value') }}
        </span>
    @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Order</label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Sort Number', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>


<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($featureValue)) ? ($featureValue->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
