<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Brand * </label>
    <div class="col-md-9">
        {!! Form::text('brand', $brand->name, ['class' => 'form-control m-b-5','placeholder'=>'Enter Name', 'data-parsley-required'=>'true', 'disabled']) !!}
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Product Name * </label>
    <div class="col-md-9">
        {!! Form::text('product_name', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Product Name', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('product_name'))
            <span class="error-form">
            * {{ $errors->first('product_name') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Product Code * </label>
    <div class="col-md-9">
        {!! Form::text('product_code', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Product Code', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('product_code'))
            <span class="error-form">
            * {{ $errors->first('product_code') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Model Number * </label>
    <div class="col-md-9">
        {!! Form::text('model_number', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Model Number', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('model_number'))
            <span class="error-form">
            * {{ $errors->first('model_number') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Price * </label>
    <div class="col-md-9">
        {!! Form::number('price', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Price', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('price'))
            <span class="error-form">
            * {{ $errors->first('price') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Discounted Price </label>
    <div class="col-md-9">
        {!! Form::number('discounted_price', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Discounted Price', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('discounted_price'))
            <span class="error-form">
            * {{ $errors->first('discounted_price') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Category * </label>
    <div class="col-md-9">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control m-b-5','placeholder'=>'Select Category', 'data-parsley-required'=>'true', 'data-url' => 'backend/form/product/ajax/sub-categories-via-category', 'id' => 'category_id']) !!}
        @if ($errors->has('category_id'))
            <span class="error-form">
            * {{ $errors->first('category_id') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sub Category</label>
    <div class="col-md-9">
        {!! Form::select('sub_category_id', $subCategories ?? [], null, ['class' => 'form-control m-b-5','placeholder'=>'Select Sub Category', 'data-parsley-required'=>'true','data-url' => 'backend/form/product/ajax/sub-category-types-via-sub-category', 'id' => 'sub_category_id']) !!}
        @if ($errors->has('sub_category_id'))
            <span class="error-form">
            * {{ $errors->first('sub_category_id') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sub Category Type </label>
    <div class="col-md-9">
        {!! Form::select('sub_category_type_id',$subCategoryTypes ?? [], null, ['class' => 'form-control m-b-5','placeholder'=>'Select Sub Category Type', 'data-parsley-required'=>'true', 'id' => 'sub_category_type_id']) !!}
        @if ($errors->has('sub_category_type_id'))
            <span class="error-form">
            * {{ $errors->first('sub_category_type_id') }}
        </span>
        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Search Query String * </label>
    <div class="col-md-9">
        {!! Form::textArea('search_query_string', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Search String', 'data-parsley-required'=>'true', 'rows' => 3]) !!}
        @if ($errors->has('search_query_string'))
            <span class="error-form">
            * {{ $errors->first('search_query_string') }}
        </span>
        @endif
    </div>
</div>


{{--<div class="form-group row m-b-15">--}}
    {{--<label class="col-form-label col-md-3">New Period </label>--}}
    {{--<div class="col-md-9">--}}
        {{--<div class="input-group date" id="datepicker-disabled-past" data-date-format="dd-mm-yyyy" data-date-start-date="Date.default">--}}
            {{--<input type="text" class="form-control" placeholder="Select Date">--}}
            {{--<span class="input-group-addon"><i class="fa fa-calendar"></i></span>--}}
        {{--</div>--}}
        {{--@if ($errors->has('sub_category_type_id'))--}}
            {{--<span class="error-form">--}}
            {{--* {{ $errors->first('sub_category_type_id') }}--}}
        {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Featured </label>
    <div class="col-md-9">
        {!! Form::select('featured', [0=>'No',1=>'Yes'], null, ['class' => 'form-control m-b-5', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('featured'))
            <span class="error-form">
            * {{ $errors->first('featured') }}
        </span>
        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Featured Image  @if(!isset($product))* @endif</label>
    <div class="col-md-9">
        {!! Form::file('featured_image', ['class' => 'form-control m-b-5','id'=>"imgInp"]) !!}
        @if ($errors->has('featured_image'))
            <span class="error-form">
            * {{ $errors->first('featured_image') }}
        </span>
        @endif
        @if(isset($product))
            <img id="blah" src="{{url($product->featured_image)}}" alt="image" class="m-t-2" width="150"/>

        @else
            <img id="blah" src="#" alt="image" class="d-none m-t-2" width="150"/>

        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label for="image_path" class="col-form-label col-md-3">Upload Image/s</label>

    <div class="col-md-8">
        @if(Request::segment(4)==='edit')
            @foreach($product->productImages as $image)
                @if(!empty($image->image_path))

                    <?php
                    $food_multiImage_path = $image->image_path;
                    ?>
                    <div class="delete-hover-image">
                        {{ Html::image($image->image_path,'',['width'=>'100px','class' =>'hover-dlt-image']) }}
                        <div class="middle">
                            <button type="button" class="btn btn-danger btn-xs delete-product-image" data-url="{{url('backend/product-image-delete')}}" data-id="{{ $image->id }}"><i class="fa fa-trash"></i></button>
                          </div>
                    </div>

                @endif
            @endforeach
            <input name="image_path[]" type="file" multiple="multiple" class="form-control m-b-5"/>
        @else
            <input name="image_path[]" type="file" multiple="multiple" class="form-control m-b-5"/>
        @endif
    </div>

</div>

<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Availability</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('availability',1, (isset($product)) ? ($product->availability == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}

        </div>

    </div>
</div>


<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($product)) ? ($product->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>



@section('script')


    <script>


//        $('.select-feature').select2();
//        $('.select-feature-values').select2();

        $(document).on("change", "#category_id", function (e) {
            e.preventDefault();
            var current_form = $(this);

            var URL = APP_URL + '/' + current_form.attr('data-url')+'/'+current_form.val();

            $('#sub_category_id').empty();
            $('#sub_category_id').append('<option value="">Select Sub Category</option>');

            $('#sub_category_type_id').empty();
            $('#sub_category_type_id').append('<option value="">Select Sub Category Type</option>');

            if(current_form.val() != ''){
                $.get(URL, function(response){
                    console.log(response);
                    $.each(response, function(index, item) {
                        $('#sub_category_id').append('<option value="'+index+'">'+item+'</option>');
                    });
                });
            }

        });


        $(document).on("change", "#sub_category_id", function (e) {
            e.preventDefault();
            var current_form = $(this);

            var URL = APP_URL + '/' + current_form.attr('data-url')+'/'+current_form.val();


            $('#sub_category_type_id').empty();
            $('#sub_category_type_id').append('<option value="">Select Sub Category Type</option>');

            if(current_form.val() != ''){
                $.get(URL, function(response){
                    console.log(response);
                    $.each(response, function(index, item) {
                        $('#sub_category_type_id').append('<option value="'+index+'">'+item+'</option>');
                    });
                });
            }

        });


        $(document).on("change", ".select-feature", function (e) {
            e.preventDefault();
            var current_form = $(this);

            var URL = APP_URL + '/' + current_form.attr('data-url')+'/'+current_form.val();


            current_form.parent().parent().find('.select-feature-values').select2('destroy');
            current_form.parent().parent().find('.select-feature-values').empty();

            current_form.parent().parent().find('.select-feature-values').append('<option value="" selected>Select Feature Value</option>');

            if(current_form.val() != ''){
                $.get(URL, function(response){
                    console.log(response);
                    $.each(response, function(index, item) {
                        console.log(item+'check');
                        current_form.parent().parent().find('.select-feature-values').append('<option value="'+item+'">'+item+'</option>');
                    });
                });
            }
            current_form.parent().parent().find('.select-feature-values').select2({tags:true});

        });


        $(document).on('click','.add-feature',function(){
            $('.select2').select2('destroy');
            $('.select2-tags').select2('destroy');

            var copyDiv = $('.first-feature').clone();
            $(copyDiv).removeClass('first-feature');
            $(copyDiv).find('.delete-feature').removeClass('d-none');


            $('.feature-div').append(copyDiv);

//            $('.select-feature').empty();
            $('.select-feature.select2').select2();
//            $('.select-feature-values').empty();
            $('.select-feature-values.select2-tags').select2({tags:true});

        });
        $(document).on('click','.delete-feature',function(){
            $(this).parent().parent().remove();

        });

        $(document).on('click','.delete-product-image',function(e){
            e.preventDefault();
            let buttonDiv = $(this);
            var url = $(this).attr('data-url')+'/'+$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Image!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((value) => {
                var request_data = {};

                request_data["_token"] = '{{ csrf_token() }}';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: request_data,
                    success: function (data) {
                        console.log(data);
                        if (data.status == "success") {
                            // location.reload();
                            swal("Deleted!", "Deleted Successfully!", "success");
                            buttonDiv.parent().parent().remove();

                        }
                        if (data.status === "fail") {
                            swal("Cannot Delete!", data.message, "error");
                        }
                    },
                });


            });

            return false;
        });
    </script>


@endsection
