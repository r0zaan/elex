

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-inverse">

                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title">Product Features</h4>

                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                               data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                               data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body">

                        <div class="form-group row m-b-15">




                            <label class="col-form-label col-md-3">Product Features</label>
                            <div class="col-lg-9">
                                <div class="feature-div">
                                    @if(Form::old('product_feature'))
                                        @foreach(old('product_feature') as $key => $val)
                                            <div class="row  @if($key == 0) first-feature @endif">
                                                <div class="col-md-3">
                                                    <select name="product_feature[]" id=""  class="form-control select-feature select2 m-b-5"  data-url="backend/form/product/ajax/feature_values-via-feature">
                                                        <option value="" selected>Select Feature</option>
                                                        @foreach($productFeatures as $featureId => $featureName)
                                                            <option value="{{$featureId}}" @if(old('product_feature.'.$key) == $featureId) selected @endif>{{$featureName}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{--{{ Form::select('product_feature[]',$productFeatures, old('product_feature.'.$key), ['class' => 'form-control select-feature select2 m-b-5','placeholder'=>'Select Feature', 'data-parsley-required'=>'true']) }}--}}
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="feature_value[]" id=""  class="form-control select-feature-values select2-tags m-b-5" >
                                                        <option value="" selected>Select Feature Value</option>

                                                        @if(old('product_feature.'.$key))
                                                            @if(\App\Models\ProductFeature::find(old('product_feature.'.$key))->productFeatureValues()->count()>0)
                                                                @if(!in_array(old('feature_value.'.$key), \App\Models\ProductFeature::find(old('product_feature.'.$key))->productFeatureValues()->pluck('value')->toArray()))
                                                                    <option value="{{old('feature_value.'.$key)}}" selected>{{old('feature_value.'.$key)}}</option>
                                                                @endif
                                                                @foreach(\App\Models\ProductFeature::find(old('product_feature.'.$key))->productFeatureValues as $value)
                                                                    <option value='{{$value->value}}' @if(old('feature_value.'.$key) == $value->value) selected @endif>{{$value->value}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value='{{old('feature_value.'.$key)}}'  selected>{{old('feature_value.'.$key)}}</option>
                                                            @endif
                                                        @else
                                                            <option value='{{old('feature_value.'.$key)}}'  selected>{{old('feature_value.'.$key)}}</option>
                                                        @endif
                                                    </select>

                                                    {{--{!! Form::select('feature_value[]',[], null, ['class' => 'form-control  select-feature-values select2-tags m-b-5','placeholder'=>'Select Feature Value', 'data-parsley-required'=>'true']) !!}--}}
                                                </div>
                                                <div class="col-md-2">
                                                    {!! Form::number('sort_number[]', old('sort_number.'.$key), ['class' => 'form-control m-b-5','placeholder'=>'Sort Position', 'data-parsley-required'=>'true']) !!}
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-sm btn-danger delete-feature @if($key == 0) d-none @endif"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        @if(isset($product))
                                            @foreach($product->productSpecifications()->get() as $key => $specification)
                                            <div class="row  @if($key == 0) first-feature @endif">
                                                <div class="col-md-3">
                                                    <select name="product_feature[]" id=""  class="form-control select-feature select2 m-b-5"  data-url="backend/form/product/ajax/feature_values-via-feature">
                                                        <option value="" selected>Select Feature</option>
                                                        @foreach($productFeatures as $featureId => $featureName)
                                                            <option value="{{$featureId}}" @if($specification->product_feature_id == $featureId) selected @endif>{{$featureName}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{--{{ Form::select('product_feature[]',$productFeatures, old('product_feature.'.$key), ['class' => 'form-control select-feature select2 m-b-5','placeholder'=>'Select Feature', 'data-parsley-required'=>'true']) }}--}}
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="feature_value[]" id=""  class="form-control select-feature-values select2-tags m-b-5" >
                                                        <option value="" selected>Select Feature Value</option>

                                                        @if($specification->product_feature_id)
                                                            @if(\App\Models\ProductFeature::find($specification->product_feature_id)->productFeatureValues()->count()>0)
                                                                @if(!in_array($specification->value , \App\Models\ProductFeature::find($specification->product_feature_id)->productFeatureValues()->pluck('value')->toArray()))
                                                                    <option value="{{$specification->value}}" selected>{{$specification->value}}</option>
                                                                @endif
                                                                @foreach(\App\Models\ProductFeature::find($specification->product_feature_id)->productFeatureValues as $value)
                                                                    <option value='{{$value->value}}' @if($specification->value == $value->value) selected @endif>{{$value->value}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value='{{$specification->value}}'  selected>{{$specification->value}}</option>
                                                            @endif
                                                        @else
                                                            <option value='{{$specification->value}}'  selected>{{$specification->value}}</option>
                                                        @endif
                                                    </select>

                                                    {{--{!! Form::select('feature_value[]',[], null, ['class' => 'form-control  select-feature-values select2-tags m-b-5','placeholder'=>'Select Feature Value', 'data-parsley-required'=>'true']) !!}--}}
                                                </div>
                                                <div class="col-md-2">
                                                    {!! Form::number('sort_number[]', $specification->sort_number, ['class' => 'form-control m-b-5','placeholder'=>'Sort Position', 'data-parsley-required'=>'true']) !!}
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-sm btn-danger delete-feature @if($key == 0) d-none @endif"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        @endforeach
                                       @else
                                        <div class="row first-feature">
                                            <div class="col-md-3">
                                                {!! Form::select('product_feature[]',$productFeatures, null, ['class' => 'form-control select-feature select2 m-b-5','placeholder'=>'Select Feature', 'data-parsley-required'=>'true', 'data-url'=>'backend/form/product/ajax/feature_values-via-feature']) !!}
                                            </div>
                                            <div class="col-md-3">
                                                {!! Form::select('feature_value[]',[], null, ['class' => 'form-control  select-feature-values select2-tags m-b-5','placeholder'=>'Select Feature Value', 'data-parsley-required'=>'true']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                {!! Form::number('sort_number[]', null, ['class' => 'form-control m-b-5','placeholder'=>'Sort Position', 'data-parsley-required'=>'true']) !!}
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-sm btn-danger delete-feature d-none"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                </div>

                                @if ($errors->has('product_feature[]'))
                                    <div class="error-add-size-message"><span class="help-block">
                                            <strong>
                                                * {{ $errors->first('product_feature[]') }}</strong>
                                        </span>
                                    </div>
                                @endif

                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-sm btn-primary add-feature"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>




                        </div>

                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
