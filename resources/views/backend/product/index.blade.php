
@extends('backend.layouts.master')

@section('content')

    <div style=" margin-left: 220px;padding: 20px 30px;">
        @include('backend.layouts.brandMenu')
    </div>



    <div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Products</a></li>
        <li class="breadcrumb-item active">All</li>
    </ol>
    <!-- end breadcrumb -->

        <h1 class="page-header">
            <img src="{{url($brand->logo)}}" alt="" style="max-height: 3rem;
    max-width: 15rem;">
        </h1>

    <!-- begin page-header -->
    <h1 class="page-header">Products <a class="btn btn-sm btn-success pull-right" href="{{route('products.create', ['brand_id' => $brand->id])}}"><i class="fa fa-plus"></i> Add Product</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Table</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th class="text-nowrap">Image</th>
                        <th class="text-nowrap">Name</th>
                        <th class="text-nowrap">Product Code</th>
                        <th class="text-nowrap">Model Number</th>
                        <th class="text-nowrap">Brand</th>
                        <th class="text-nowrap">Category</th>
                        <th class="text-nowrap">Sub Category</th>
                        <th class="text-nowrap">Sub Category Type</th>
                        <th class="text-nowrap">Featured</th>
                        <th class="text-nowrap">Availability</th>
                        <th class="text-nowrap">Status</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr class="odd gradeX">
                        <td width="1%" class="f-w-600 text-inverse">{{$loop->iteration}}</td>
                        <td>{{Html::image($product->featured_image, '', ['width' => '200px'])}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->product_code}}</td>
                        <td>{{$product->model_number}}</td>
                        <td>{{$product->brand->name ?? null}}</td>
                        <td>{{$product->category->name ?? null}}</td>
                        <td>{{$product->subCategory->name ?? null}}</td>
                        <td>{{$product->subCategoryType->name ?? null}}</td>
                        <td>{!!($product->featured == 1) ? 'Yes' : 'No'!!} </td>
                        <td>{!!($product->availability == 1) ? '<span class="label label-green">In Stock</span>' : '<span class="label label-danger">Out of Stock</span>'!!} </td>
                        <td>{!!($product->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!} </td>
                        <td class="with-btn-group" nowrap="">
                            <div class="btn-group">
                                {{--<a href="{{route('products.show',[$product->id, 'brand_id' => $brand->id])}}" class="btn btn-info btn-sm width-90"><i class="fa fa-eye"></i> View</a>--}}
                                <a href="#" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" style="">
                                    <a href="{{route('products.edit',[$product->id, 'brand_id' => $brand->id])}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit</a>
                                    <form action="{{ route('products.destroy', [$product->id, 'brand_id' => $brand->id]) }}"
                                        method="DELETE" class="delete-data-ajax">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="dropdown-item" title="Delete"><i class="fa fa-trash"></i> Delete</button>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>


@endsection


