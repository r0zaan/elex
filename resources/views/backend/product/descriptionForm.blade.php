<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-inverse">

            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Descriptions</h4>

                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                       data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Short Description </label>
                    <div class="col-md-9">
                        {!! Form::textArea('short_description', null, ['class' => 'form-control m-b-5 ckeditor','placeholder'=>'Enter Short Description', 'data-parsley-required'=>'true']) !!}
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Long Description </label>
                    <div class="col-md-9">
                        {!! Form::textArea('long_description', null, ['class' => 'form-control m-b-5 ckeditor','placeholder'=>'Enter Long Description', 'data-parsley-required'=>'true']) !!}
                    </div>
                </div>


            </div>
            <!-- end panel-body -->
        </div>
    </div>
</div>
