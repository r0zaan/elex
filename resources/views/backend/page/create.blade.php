
@extends('backend.layouts.master')

@section('content')


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Page</a></li>
        <li class="breadcrumb-item active">All</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Create Pages <a class="btn btn-sm btn-success" href="{{route('pages.index')}}"><i class="fa fa-arrow-left"></i> Back</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    {{Form::open(['method' => 'POST','route' => 'pages.store','data-parsley-validate'=>'true'])}}

    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-inverse">

                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                    @include('backend.page.form')


            </div>
            <!-- end panel-body -->
        </div>
        </div>
        <div class="col-lg-5">
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Meta Tag Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                    @include('backend.layouts.metaTag.form')


            </div>
            <!-- end panel-body -->
        </div>

        </div>

            <div class="col-md-8 col-sm-8">
                <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Create</button>
            </div>
    </div>

    {{Form::close()}}
    <!-- end panel -->
</div>


@endsection
