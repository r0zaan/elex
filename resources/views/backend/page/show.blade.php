
@extends('backend.layouts.master')

@section('content')


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Page</a></li>
        <li class="breadcrumb-item active">All</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Show Pages <a class="btn btn-sm btn-success" href="{{route('pages.index')}}"><i class="fa fa-arrow-left"></i> Back</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->

    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-inverse">

                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Title</label>
                    <div class="col-md-9">
                        {{$page->title}}
                    </div>
                </div>
                @if(isset($page))
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Slug</label>
                    <div class="col-md-9">
                        {{$page->slug}}
                    </div>
                </div>
                @endif
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Body</label>
                    <div class="col-md-9">
                        {!!$page->body!!}
                    </div>
                </div>
                <div class="form-group row m-b-10">
                    <label class="col-md-3 col-form-label">Status</label>
                    {!!($page->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!}
                </div>


            </div>
            <!-- end panel-body -->
        </div>
        </div>
        <div class="col-lg-5">
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Meta Tag Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                    @include('backend.layouts.metaTag.show',['data' => $page])


            </div>
            <!-- end panel-body -->
        </div>

        </div>

            <div class="col-md-8 col-sm-8">
                <a class="btn btn-sm btn-success" href="{{route('pages.index')}}"><i class="fa fa-arrow-left"></i> Back</a>
         </div>
    </div>


    <!-- end panel -->
</div>


@endsection
