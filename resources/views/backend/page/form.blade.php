<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Title * </label>
    <div class="col-md-9">
        {!! Form::text('title', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Title', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('title'))
        <span class="error-form">
            * {{ $errors->first('title') }}
        </span>
    @endif
    </div>
</div>
@if(isset($page))
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Slug</label>
    <div class="col-md-9">
        {!! Form::text('slug', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter slug',  'data-parsley-required'=>'true']) !!}
        @if ($errors->has('slug'))
        <span class="error-form">
            * {{ $errors->first('slug') }}
        </span>
    @endif
    </div>
</div>
@endif
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Body</label>
    <div class="col-md-9">
        {!! Form::textarea('body', null, ['class' => 'form-control m-b-5 ckeditor','placeholder'=>'Enter Body', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('body'))
        <span class="error-form">
            * {{ $errors->first('body') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($page)) ? ($page->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
