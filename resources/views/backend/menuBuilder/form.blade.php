
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Name</label>
    <div class="col-md-9">
        {!! Form::text('name', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Name']) !!}
         @if ($errors->has('name'))
        <span class="error-form">
            * {{ $errors->first('name') }}
        </span>
    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Number</label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5']) !!}
         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Parent</label>
    <div class="col-md-9">
        {!! Form::select('parent_id',$parents,null, ['class' => 'form-control m-b-5 select2 parent_select','placeholder'=>'Select Parent Menu (if not leave it)','data-url' => 'backend/menuBuilders/parent']) !!}
         @if ($errors->has('parent_id'))
        <span class="error-form">
            * {{ $errors->first('parent_id') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Type</label>
    <div class="col-md-9">
        {!! Form::select('type',isset($menuBuilder) ? $types : ['Large Dropdown' => 'Large Dropdown','Brand' => 'Brand','Dropdown' => 'Dropdown','Link' => 'Link','Slug' => 'Slug'], null, ['class' => 'form-control m-b-5 type_select']) !!}
         @if ($errors->has('type'))
        <span class="error-form">
            * {{ $errors->first('type') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Go To</label>
    <div class="col-md-9">
        {!! Form::text('go_to', null, ['class' => 'form-control m-b-5','placeholder'=>'Go to Link/Slug', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('go_to'))
        <span class="error-form">
            * {{ $errors->first('go_to') }}
        </span>
        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Land in Same Page?</label>
    <div class="col-md-9">
        {!! Form::select('same_page',['1' => 'Yes' ,'0' => 'No'], null, ['class' => 'form-control m-b-5', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('same_page'))
        <span class="error-form">
            * {{ $errors->first('same_page') }}
        </span>
        @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($menuBuilder)) ? ($menuBuilder->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>


@section('script')

<script>
    $(document).on('change','.parent_select',function(){
        var current_form = $(this);

    var URL = APP_URL + '/' + current_form.attr('data-url')+'/'+current_form.val();

        $('.type_select').empty();
        if(current_form.val() != ''){
            $.get(URL, function(response){
                console.log(response);
                if(response == "Large Dropdown"){
                    $('.type_select').append('<option value="Dropdown">Dropdown</option>');
                    $('.type_select').append('<option value="Slug">Slug</option>');
                    $('.type_select').append('<option value="Link">Link</option>');
                }else if(response == "Dropdown"){
                    $('.type_select').append('<option value="Slug">Slug</option>');
                    $('.type_select').append('<option value="Link">Link</option>');
                }else{
                    $('.type_select').append('<option value="Large Dropdown">Large Dropdown</option>');
                    $('.type_select').append('<option value="Brand">Brand</option>');
                    $('.type_select').append('<option value="Dropdown">Dropdown</option>');
                    $('.type_select').append('<option value="Slug">Slug</option>');
                    $('.type_select').append('<option value="Link">Link</option>');
                }
            });
        }else{
            $('.type_select').append('<option value="Large Dropdown">Large Dropdown</option>');
                    $('.type_select').append('<option value="Brand">Brand</option>');
                    $('.type_select').append('<option value="Dropdown">Dropdown</option>');
                    $('.type_select').append('<option value="Slug">Slug</option>');
                    $('.type_select').append('<option value="Link">Link</option>');
        }


    });


</script>
@endsection
