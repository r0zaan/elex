<tr class="odd gradeX">
    <td width="5%" class="f-w-600 text-inverse">{{(isset($keyDataTwo) ? $keyDataTwo.' . ' : "").(isset($keyData) ? $keyData.' . ' : "") .$loop->iteration}}</td>
    <td>{{$menuBuilder->sort_number}}</td>
    <td>{{$menuBuilder->name}}</td>
    <td>{{$menuBuilder->type}}</td>
    <td>{{$menuBuilder->go_to}}</td>
    <td>{!!($menuBuilder->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!} </td>
    <td class="with-btn-group" nowrap="">
        <div class="btn-group">
            <a href="{{route('menuBuilders.show',$menuBuilder->id)}}" class="btn btn-info btn-sm width-90"><i class="fa fa-eye"></i> View</a>
            <a href="#" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown" aria-expanded="false">
            <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" style="">
                <a href="{{route('menuBuilders.copy',$menuBuilder->id)}}" class="dropdown-item"><i class="fa fa-copy"></i> Copy</a>
                <a href="{{route('menuBuilders.edit',$menuBuilder->id)}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit</a>
                <form action="{{ route('menuBuilders.destroy', [$menuBuilder->id]) }}"
                    method="DELETE" class="delete-data-ajax">
                    {!! csrf_field() !!}
                    <button type="submit" class="dropdown-item" title="Delete"><i class="fa fa-trash"></i> Delete</button>
                    </button>
                </form>

            </div>
        </div>
    </td>
</tr>
