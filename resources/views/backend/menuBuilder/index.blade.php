
@extends('backend.layouts.master')

@section('content')


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">MainPage</a></li>
        <li class="breadcrumb-item active">Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Menu <a class="btn btn-sm btn-success" href="{{route('menuBuilders.create')}}"><i class="fa fa-plus"></i> Add Menu</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Table</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <table id="menuBuilder" class="table table-striped table-bordered table-td-valign-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th class="text-nowrap">Sort Number</th>
                        <th class="text-nowrap">Name</th>
                        <th class="text-nowrap">Type</th>
                        <th class="text-nowrap">Go to</th>
                        <th class="text-nowrap">Status</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($menuBuilders as $key => $menuBuilder)
                        @include('backend.menuBuilder.menuIndex',['menuBuilder' => $menuBuilder])
                        @if( $menuBuilder->childMenus()->count() > 0)
                            @foreach( $menuBuilder->childMenus()->orderBy('sort_number','ASC')->get() as $keyTwo => $menuBuilderChild)
                            @include('backend.menuBuilder.menuIndex',['menuBuilder' => $menuBuilderChild,'keyData' => $key+1])
                            @if( $menuBuilderChild->childMenus()->count() > 0)
                            @foreach( $menuBuilderChild->childMenus()->orderBy('sort_number','ASC')->get() as $menuBuilderChildSecond)
                            @include('backend.menuBuilder.menuIndex',['menuBuilder' => $menuBuilderChildSecond,'keyDataTwo' => $key+1,'keyData' => $keyTwo+1])
                            @endforeach
                            {{-- <tr class="odd gradeX">
                                <td width="3%" class="f-w-600 text-inverse">{{$key+1 .'.'.$loop->iteration}}</td>
                                <td>{{$menuBuilderChild->sort_number}}</td>
                                <td>{{$menuBuilderChild->name}}</td>
                                <td>{{$menuBuilderChild->type}}</td>
                                <td>{{$menuBuilderChild->go_to}}</td>
                                <td>{!!($menuBuilderChild->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!} </td>
                                <td class="with-btn-group" nowrap="">
                                    <div class="btn-group">
                                        <a href="{{route('menuBuilders.show',$menuBuilderChild->id)}}" class="btn btn-info btn-sm width-90"><i class="fa fa-eye"></i> View</a>
                                        <a href="#" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                            <a href="{{route('menuBuilders.copy',$menuBuilderChild->id)}}" class="dropdown-item"><i class="fa fa-copy"></i> Copy</a>
                                            <a href="{{route('menuBuilders.edit',$menuBuilderChild->id)}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit</a>
                                            <form action="{{ route('menuBuilders.destroy', [$menuBuilderChild->id]) }}"
                                                method="DELETE" class="delete-data-ajax">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="dropdown-item" title="Delete"><i class="fa fa-trash"></i> Delete</button>
                                                </button>
                                            </form>

                                        </div>
                                    </div>
                                </td>
                            </tr> --}}
                            @endif
                        @endforeach
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>


@endsection
@section('script')
    <script>
        $('#menuBuilder').dataTable( {
                "pageLength": 50
                } );
    </script>
@endsection
