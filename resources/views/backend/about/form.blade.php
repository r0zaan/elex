<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Title * </label>
    <div class="col-md-9">
        {!! Form::text('title', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Title', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('title'))
        <span class="error-form">
            * {{ $errors->first('title') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Image @if(!isset($about))* @endif</label>
    <div class="col-md-9">
        {!! Form::file('image', ['class' => 'form-control m-b-5','id'=>"imgInp"]) !!}
         @if ($errors->has('image'))
        <span class="error-form">
            * {{ $errors->first('image') }}
        </span>
    @endif
    @if(isset($about))
    <img id="blah" src="{{url($about->image)}}" alt="image" class="m-t-2" width="400"/>

    @else
    <img id="blah" src="#" alt="image" class="d-none m-t-2" width="400"/>

    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Number * </label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5','placeholder'=>'Sort Number', 'data-parsley-required'=>'true']) !!}
         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Body</label>
    <div class="col-md-9">
        {!! Form::textarea('body', null, ['class' => 'form-control m-b-5 ','placeholder'=>'Enter Body', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('body'))
        <span class="error-form">
            * {{ $errors->first('body') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($about)) ? ($about->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
