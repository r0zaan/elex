<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Login | CG Electronics</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <link
    rel="icon"
    type="image/png"
    href="{{asset('assets/img/icon/cg-inspiring-life-final-vector-logo.png')}}"
  />
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{asset('colorAdmin/admin/template/assets/css/default/app.min.css')}}" rel="stylesheet" />



	<link href="{{asset('colorAdmin/admin/template/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/custom/backend.css')}}" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show">
		<span class="spinner"></span>
	</div>
	<!-- end #page-loader -->

	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-v1">
			<!-- begin login-container -->
			<div class="login-container">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
                        <img
                        src="{{url('assets/img/brand/CG-Electronics-New.png')}}"
                        height="100%"
                        width="200"
                      />

					</div>
					<div class="icon">
						<i class="fa fa-lock"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-body -->
				<div class="login-body">
					<!-- begin login-content -->
					<div class="login-content">
						<form action="{{route('admin.login.post')}}" method="POST" class="margin-bottom-0">
                            {{csrf_field()}}
							<div class="form-group m-b-20">
								<input type="email" name="email" class="form-control form-control-lg inverse-mode" placeholder="Email Address" required />
							</div>
							<div class="form-group m-b-20">
								<input type="password" name="password" class="form-control form-control-lg inverse-mode" placeholder="Password" required />
							</div>
							<div class="checkbox checkbox-css m-b-20">
								<input type="checkbox" id="remember_checkbox" />
								<label for="remember_checkbox">
								Remember Me
								</label>
							</div>
							<div class="login-buttons">
								<button type="submit" class="btn btn-company btn-block btn-lg">Sign me in</button>
							</div>
						</form>
					</div>
					<!-- end login-content -->
				</div>
				<!-- end login-body -->
			</div>
			<!-- end login-container -->
		</div>
		<!-- end login -->



	</div>
	<!-- end page container -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('colorAdmin/admin/template/assets/js/app.min.js')}}"></script>
	<script src="{{asset('colorAdmin/admin/template/assets/js/theme/default.min.js')}}"></script>


	<script src="{{asset('colorAdmin/admin/template/assets/plugins/gritter/js/jquery.gritter.js')}}"></script>

    @include('backend.layouts.message')
	<!-- ================== END BASE JS ================== -->
</body>
</html>
