
@extends('backend.layouts.master')

@section('content')

    <div style=" margin-left: 220px;padding: 20px 30px;">
        @include('backend.layouts.brandMenu')
    </div>


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Category</a></li>
        <li class="breadcrumb-item active">Show</li>
    </ol>
    <!-- end breadcrumb -->

    <h1 class="page-header">
        <img src="{{url($brand->logo)}}" alt="" style="max-height: 3rem;
    max-width: 15rem;">
    </h1>

    <!-- begin page-header -->
    <h1 class="page-header">Show Category <a class="btn btn-sm btn-success pull-right" href="{{route('categories.index', ['brand_id' => $brand->id])}}"><i class="fa fa-arrow-left"></i> Back</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->

    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-inverse">

                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Brand</label>
                    <div class="col-md-9">
                        {{$category->brand->name}}
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Category</label>
                    <div class="col-md-9">
                        {{$category->name}}
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Code</label>
                    <div class="col-md-9">
                        {{$category->code}}
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Slug</label>
                    <div class="col-md-9">
                        {{$category->slug}}
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Body</label>
                    <div class="col-md-9">
                        {!!$category->description!!}
                    </div>
                </div>
                <div class="form-group row m-b-10">
                    <label class="col-md-3 col-form-label">Status</label>
                    {!!($category->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!}
                </div>


            </div>
            <!-- end panel-body -->
        </div>
        </div>
        <div class="col-lg-5">
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Meta Tag Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                    @include('backend.layouts.metaTag.show',['data' => $category])


            </div>
            <!-- end panel-body -->
        </div>

        </div>

            <div class="col-md-8 col-sm-8">
                <a class="btn btn-sm btn-success" href="{{route('categories.index', ['brand_id' => $brand->id])}}"><i class="fa fa-arrow-left"></i> Back</a>
         </div>
    </div>

    <!-- end panel -->
</div>


@endsection
