<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Brand * </label>
    <div class="col-md-9">
        {{--{!! Form::select('brand_id', $brands, null, ['class' => 'form-control m-b-5','placeholder'=>'Select Brand', 'data-parsley-required'=>'true', 'disabled']) !!}--}}
        {!! Form::text('brand', $brand->name, ['class' => 'form-control m-b-5','placeholder'=>'Select Brand', 'data-parsley-required'=>'true', 'disabled']) !!}
        {{Form::hidden('brand_id', $brand->id)}}
        @if ($errors->has('brand_id'))
            <span class="error-form">
            * {{ $errors->first('brand_id') }}
        </span>
        @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Category Name * </label>
    <div class="col-md-9">
        {!! Form::text('name', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Name', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('name'))
        <span class="error-form">
            * {{ $errors->first('name') }}
        </span>
    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Code * </label>
    <div class="col-md-9">
        {!! Form::text('code', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Code', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 word)</small>
         @if ($errors->has('code'))
        <span class="error-form">
            * {{ $errors->first('code') }}
        </span>
    @endif
    </div>
</div>


<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Order</label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Sort Number', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>

@if(isset($category))
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Slug</label>
    <div class="col-md-9">
        {!! Form::text('slug', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter slug',  'data-parsley-required'=>'true']) !!}
        @if ($errors->has('slug'))
        <span class="error-form">
            * {{ $errors->first('slug') }}
        </span>
        @endif
    </div>
</div>
@endif

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Description</label>
    <div class="col-md-9">
        {!! Form::textarea('description', null, ['class' => 'form-control m-b-5 ckeditor','placeholder'=>'Enter Description', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('description'))
        <span class="error-form">
            * {{ $errors->first('description') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($category)) ? ($category->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
