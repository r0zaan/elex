
@extends('backend.layouts.master')

@section('content')

    <div style=" margin-left: 220px;padding: 20px 30px;">
        @include('backend.layouts.brandMenu')
    </div>


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">MainPage</a></li>
        <li class="breadcrumb-item active">{{$type}} Slider</li>
    </ol>
    <!-- end breadcrumb -->

    <h1 class="page-header">
        <img src="{{url($brand->logo)}}" alt="" style="max-height: 3rem;
    max-width: 15rem;">
    </h1>

    <!-- begin page-header -->
    <h1 class="page-header">{{$type}} Slider <a class="btn btn-sm btn-success pull-right" href="{{route('brandPageSliders.create', ['type'=>$type, 'brand_id' => $brand->id])}}"><i class="fa fa-plus"></i> Add Slider</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Table</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        @if($type != 'Brand')
                            <th class="text-nowrap">Category</th>
                        @endif
                        <th class="text-nowrap">Sort Number</th>
                        <th class="text-nowrap">Image</th>
                        <th class="text-nowrap">Type</th>
                        <th class="text-nowrap">Go to</th>
                        <th class="text-nowrap">Status</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sliders as $slider)
                    <tr class="odd gradeX">
                        <td width="1%" class="f-w-600 text-inverse">{{$loop->iteration}}</td>
                        @if($type != 'Brand')
                            <th class="text-nowrap">
                                @if($type == 'Category')
                                    {{\App\Models\Category::find($slider->slider_type_parent_id)->name}}
                                @elseif($type == 'Sub Category')
                                    {{\App\Models\SubCategory::find($slider->slider_type_parent_id)->name.' - '.App\Models\SubCategory::find($slider->slider_type_parent_id)->category->name}}
                                @elseif($type == 'Sub Category Type')
                                    {{\App\Models\SubCategoryType::find($slider->slider_type_parent_id)->name.' - '.App\Models\SubCategoryType::find($slider->slider_type_parent_id)->subCategory->name.' - '.App\Models\SubCategoryType::find($slider->slider_type_parent_id)->subcategory->category->name}}
                                @endif
                            </th>
                        @endif
                        <td>{{$slider->sort_number}}</td>
                        <td><img src="{{url($slider->image) ?? "#"}}" width="150"/></td>
                        <td>{{$slider->link_type}}</td>
                        <td>{{$slider->go_to}}</td>
                        <td>{!!($slider->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!} </td>
                        <td class="with-btn-group" nowrap="">
                            <div class="btn-group">
                                {{--<a href="{{route('brandPageSliders.show',[$slider->id, 'type'=>$type,'brand_id'=>$brand->id])}}" class="btn btn-info btn-sm width-90"><i class="fa fa-eye"></i> View</a>--}}
                                <a href="#" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" style="">
                                    <a href="{{route('brandPageSliders.edit',[$slider->id, 'type'=>$type,'brand_id'=>$brand->id])}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit</a>
                                    <form action="{{ route('brandPageSliders.destroy', [$slider->id, 'type'=>$type,'brand_id'=>$brand->id]) }}"
                                        method="DELETE" class="delete-data-ajax">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="dropdown-item" title="Delete"><i class="fa fa-trash"></i> Delete</button>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>


@endsection
