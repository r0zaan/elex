
@extends('backend.layouts.master')

@section('content')

    <div style=" margin-left: 220px;padding: 20px 30px;">
        @include('backend.layouts.brandMenu')
    </div>


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">MainPage</a></li>
        <li class="breadcrumb-item active">Slider</li>
    </ol>
    <!-- end breadcrumb -->

    <h1 class="page-header">
        <img src="{{url($brand->logo)}}" alt="" style="max-height: 3rem;
    max-width: 15rem;">
    </h1>

    <!-- begin page-header -->
    <h1 class="page-header">Edit {{$type}} Slider <a class="btn btn-sm btn-success pull-right" href="{{route('brandPageSliders.index', ['type'=>$type,'brand_id'=>$brand->id])}}"><i class="fa fa-arrow-left"></i> Back</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    {{Form::model($brandPageSlider,['method' => 'PATCH',
        'route' => ['brandPageSliders.update', [$brandPageSlider->id, 'type'=>$type,'brand_id'=>$brand->id]],
        'data-parsley-validate'=>'true','files' => true])}}

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-inverse">

                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Form</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                    @include('backend.brandPageSlider.form')


            </div>
            <!-- end panel-body -->
        </div>
        </div>

        </div>

        <div class="col-md-8 col-sm-8">
                <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Update</button>
        </div>
    </div>

    {{Form::close()}}
    <!-- end panel -->
</div>


@endsection
