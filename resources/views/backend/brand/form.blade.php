<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Name * </label>
    <div class="col-md-9">
        {!! Form::text('name', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Name', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('name'))
        <span class="error-form">
            * {{ $errors->first('name') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Code * </label>
    <div class="col-md-9">
        {!! Form::text('code', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Code', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 word)</small>
         @if ($errors->has('code'))
        <span class="error-form">
            * {{ $errors->first('code') }}
        </span>
    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Brand Color Code * </label>
    <div class="col-md-9">
        {!! Form::text('brand_color_code', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Color Code', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 word)</small>
        @if ($errors->has('brand_color_code'))
            <span class="error-form">
            * {{ $errors->first('brand_color_code') }}
        </span>
        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Logo   @if(!isset($brand))* @endif</label>
    <div class="col-md-9">
        {!! Form::file('logo', ['class' => 'form-control m-b-5','id'=>"imgInp"]) !!}
         @if ($errors->has('logo'))
        <span class="error-form">
            * {{ $errors->first('logo') }}
        </span>
    @endif
    @if(isset($brand))
    <img id="blah" src="{{url($brand->logo)}}" alt="image" class="m-t-2" width="150"/>

    @else
    <img id="blah" src="#" alt="image" class="d-none m-t-2" width="150"/>

    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Slogan</label>
    <div class="col-md-9">
        {!! Form::text('slogan', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Slogan']) !!}
         @if ($errors->has('slogan'))
        <span class="error-form">
            * {{ $errors->first('slogan') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Order</label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Sort Number', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>
@if(isset($brand))
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Slug</label>
    <div class="col-md-9">
        {!! Form::text('slug', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter slug',  'data-parsley-required'=>'true']) !!}
        @if ($errors->has('slug'))
        <span class="error-form">
            * {{ $errors->first('slug') }}
        </span>
    @endif
    </div>
</div>
@endif

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Description</label>
    <div class="col-md-9">
        {!! Form::textarea('description', null, ['class' => 'form-control m-b-5 ckeditor','placeholder'=>'Enter Description', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('description'))
        <span class="error-form">
            * {{ $errors->first('description') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($brand)) ? ($brand->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
