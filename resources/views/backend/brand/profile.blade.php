@extends('backend.layouts.master')

@section('content')
    <div style=" margin-left: 220px;padding: 20px 30px;">
        @include('backend.layouts.brandMenu')
    </div>


    <div id="content" class="content">


        <!-- begin breadcrumb -->
        <ol class="breadcrumb float-xl-right">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">MainPage</a></li>
            <li class="breadcrumb-item active"></li>
        </ol>


        <h1 class="page-header">
            <img src="{{url($brand->logo)}}" alt="" style="max-height: 3rem;
    max-width: 15rem;">
        </h1>


        {{--<ul class="nav nav-pills mb-2">--}}
            {{--<li class="nav-item">--}}
                {{--<a href="#nav-pills-tab-1" data-toggle="tab" class="nav-link active">--}}
                    {{--<span class="d-sm-none">Pills 1</span>--}}
                    {{--<span class="d-sm-block d-none">Pills Tab 1</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="#nav-pills-tab-2" data-toggle="tab" class="nav-link">--}}
                    {{--<span class="d-sm-none">Pills 2</span>--}}
                    {{--<span class="d-sm-block d-none">Pills Tab 2</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="#nav-pills-tab-3" data-toggle="tab" class="nav-link">--}}
                    {{--<span class="d-sm-none">Pills 3</span>--}}
                    {{--<span class="d-sm-block d-none">Pills Tab 3</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="#nav-pills-tab-4" data-toggle="tab" class="nav-link">--}}
                    {{--<span class="d-sm-none">Pills 4</span>--}}
                    {{--<span class="d-sm-block d-none">Pills Tab 4</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
        {{--<!-- end nav-pills -->--}}
        {{--<!-- begin tab-content -->--}}
        {{--<div class="tab-content p-15 rounded bg-white mb-4">--}}
            {{--<!-- begin tab-pane -->--}}
            {{--<div class="tab-pane fade active show" id="nav-pills-tab-1">--}}
                {{--<h3 class="m-t-10">Nav Pills Tab 1</h3>--}}
                {{--<p>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
                    {{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
                    {{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
                    {{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
                    {{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
                    {{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
                    {{--nec eleifend orci eros id lectus.--}}
                {{--</p>--}}
            {{--</div>--}}
            {{--<!-- end tab-pane -->--}}
            {{--<!-- begin tab-pane -->--}}
            {{--<div class="tab-pane fade" id="nav-pills-tab-2">--}}
                {{--<h3 class="m-t-10">Nav Pills Tab 2</h3>--}}
                {{--<p>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
                    {{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
                    {{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
                    {{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
                    {{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
                    {{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
                    {{--nec eleifend orci eros id lectus.--}}
                {{--</p>--}}
            {{--</div>--}}
            {{--<!-- end tab-pane -->--}}
            {{--<!-- begin tab-pane -->--}}
            {{--<div class="tab-pane fade" id="nav-pills-tab-3">--}}
                {{--<h3 class="m-t-10">Nav Pills Tab 3</h3>--}}
                {{--<p>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
                    {{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
                    {{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
                    {{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
                    {{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
                    {{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
                    {{--nec eleifend orci eros id lectus.--}}
                {{--</p>--}}
            {{--</div>--}}
            {{--<!-- end tab-pane -->--}}
            {{--<!-- begin tab-pane -->--}}
            {{--<div class="tab-pane fade" id="nav-pills-tab-4">--}}
                {{--<h3 class="m-t-10">Nav Pills Tab 4</h3>--}}
                {{--<p>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
                    {{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
                    {{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
                    {{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
                    {{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
                    {{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
                    {{--nec eleifend orci eros id lectus.--}}
                {{--</p>--}}
            {{--</div>--}}
            {{--<!-- end tab-pane -->--}}
        {{--</div>--}}


    </div>


    @endsection