
@if($errors->any())
<script>
// $(window).on('load', function() {
    setTimeout(function() {
        $.gritter.add({
            title: 'Form Required',
            text: 'Validation Error',
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });
    }, 1000);
// });


</script>
<style>
    body #gritter-notice-wrapper .gritter-item-wrapper{
        background: #ff5b57;
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item p{
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item .gritter-close{
        color:white;
    }
</style>
@endif

@if(session()->has('success'))
<script>
    let message = "{{session()->get('success')}}";
    setTimeout(function() {
        $.gritter.add({
            title: 'Success',
            text: message,
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });
    }, 1000);


</script>
<style>
    body #gritter-notice-wrapper .gritter-item-wrapper{
        background: #00770a;
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item p{
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item .gritter-close{
        color:white;
    }
</style>
@endif
@if(session()->has('error'))
<script>
    let message = "{{session()->get('error')}}";
    setTimeout(function() {
        $.gritter.add({
            title: 'Success',
            text: message,
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });
    }, 1000);


</script>
<style>
    body #gritter-notice-wrapper .gritter-item-wrapper{
        background: #b92727;
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item p{
        color:white;
    }
    body #gritter-notice-wrapper .gritter-item-wrapper .gritter-item .gritter-close{
        color:white;
    }
</style>
@endif
