<!-- begin #top-menu -->
<div id="top-menu" class="top-menu" style="top: 50px; position: absolute;margin-left: 220px;z-index:0;">
    <!-- begin nav -->
    <ul class="nav">
        <li class="has-sub">
            <a href="javascript:;">
                <i class="fa fa-th-large"></i>
                <span>Product Catalog</span>
                <b class="caret"></b>
            </a>
            <ul class="sub-menu">
                <li><a href="{{route('categories.index', ['brand_id' => $brand->id])}}">Category</a></li>
                <li><a href="{{route('sub-categories.index', ['brand_id' => $brand->id])}}">Sub Category</a></li>
                <li><a href="{{route('sub-category-types.index', ['brand_id' => $brand->id])}}">Sub Category Type</a></li>
                <li><a href="{{route('product-features.index', ['brand_id' => $brand->id])}}">Product Feature</a></li>
                <li><a href="{{route('product-feature-values.index', ['brand_id' => $brand->id])}}">Product Feature Value</a></li>
                <li><a href="{{route('products.index', ['brand_id' => $brand->id])}}">Product</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <i class="fa fa-hdd"></i>
                <span>Sliders</span>
                <b class="caret"></b>
            </a>
            <ul class="sub-menu">
                <li><a href="{{route('brandPageSliders.index', ['type' => 'Brand','brand_id' => $brand->id])}}">Brand</a></li>
                <li><a href="{{route('brandPageSliders.index', ['type' => 'Category','brand_id' => $brand->id])}}">Category</a></li>
                <li><a href="{{route('brandPageSliders.index', ['type' => 'Sub Category','brand_id' => $brand->id])}}">Sub Category</a></li>
                <li><a href="{{route('brandPageSliders.index', ['type' => 'Sub Category Type','brand_id' => $brand->id])}}">Sub Category Type</a></li>
            </ul>
        </li>
        <li>
            <a href="widget.html">
                <i class="fab fa-simplybuilt"></i>
                <span>Widgets <span class="label label-theme">NEW</span></span>
            </a>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <i class="fa fa-gem"></i>
                <span>UI Elements <span class="label label-theme">NEW</span></span>
                <b class="caret"></b>
            </a>
            <ul class="sub-menu">
                <li><a href="ui_general.html">General <i class="fa fa-paper-plane text-theme"></i></a></li>
                <li><a href="ui_typography.html">Typography</a></li>
                <li><a href="ui_tabs_accordions.html">Tabs & Accordions</a></li>
                <li><a href="ui_unlimited_tabs.html">Unlimited Nav Tabs</a></li>
                <li><a href="ui_modal_notification.html">Modal & Notification <i class="fa fa-paper-plane text-theme"></i></a></li>
                <li><a href="ui_widget_boxes.html">Widget Boxes</a></li>
                <li><a href="ui_media_object.html">Media Object</a></li>
                <li><a href="ui_buttons.html">Buttons <i class="fa fa-paper-plane text-theme"></i></a></li>
                <li><a href="ui_icons.html">Icons</a></li>
                <li><a href="ui_simple_line_icons.html">Simple Line Icons</a></li>
                <li><a href="ui_ionicons.html">Ionicons</a></li>
                <li><a href="ui_tree.html">Tree View</a></li>
                <li><a href="ui_language_bar_icon.html">Language Bar & Icon</a></li>
                <li><a href="ui_social_buttons.html">Social Buttons</a></li>
                <li><a href="ui_tour.html">Intro JS</a></li>
            </ul>
        </li>
    </ul>
    <!-- end nav -->
</div>
<!-- end #top-menu -->