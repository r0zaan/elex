<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta Title</label>
    <div class="col-md-9">
       {{$data->meta_title}}
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta keywords</label>
    <div class="col-md-9">
        {{$data->meta_keywords}}
     </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta Description</label>
    <div class="col-md-9">
        {{$data->meta_description}}
    </div>
</div>
