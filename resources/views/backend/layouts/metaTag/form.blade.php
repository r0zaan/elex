<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta Title</label>
    <div class="col-md-9">
        {!! Form::text('meta_title', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Meta Title',  'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta keywords</label>
    <div class="col-md-9">
        {!! Form::textarea('meta_keywords', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Meta keywords',  'data-parsley-required'=>'true']) !!}
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Meta Description</label>
    <div class="col-md-9">
        {!! Form::textarea('meta_description', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Meta Description',  'data-parsley-required'=>'true']) !!}
    </div>
</div>
