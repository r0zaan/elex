
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Image   @if(!isset($homeSlider))* @endif</label>
    <div class="col-md-9">
        {!! Form::file('image', ['class' => 'form-control m-b-5','id'=>"imgInp"]) !!}
         @if ($errors->has('image'))
        <span class="error-form">
            * {{ $errors->first('image') }}
        </span>
    @endif
    @if(isset($homeSlider))
    <img id="blah" src="{{url($homeSlider->image)}}" alt="image" class="m-t-2" width="400"/>

    @else
    <img id="blah" src="#" alt="image" class="d-none m-t-2" width="400"/>

    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Sort Number</label>
    <div class="col-md-9">
        {!! Form::number('sort_number', null, ['class' => 'form-control m-b-5']) !!}
         @if ($errors->has('sort_number'))
        <span class="error-form">
            * {{ $errors->first('sort_number') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Link Type</label>
    <div class="col-md-9">
        {!! Form::select('type',['None' => 'None','Link' => 'Link','Slug' => 'Slug'], null, ['class' => 'form-control m-b-5']) !!}
         @if ($errors->has('type'))
        <span class="error-form">
            * {{ $errors->first('type') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Go To</label>
    <div class="col-md-9">
        {!! Form::text('go_to', null, ['class' => 'form-control m-b-5','placeholder'=>'Go to Link/Slug', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('go_to'))
        <span class="error-form">
            * {{ $errors->first('go_to') }}
        </span>
        @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Land in Same Page?</label>
    <div class="col-md-9">
        {!! Form::select('same_page',['1' => 'Yes' ,'0' => 'No'], null, ['class' => 'form-control m-b-5', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('same_page'))
        <span class="error-form">
            * {{ $errors->first('same_page') }}
        </span>
        @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($homeSlider)) ? ($homeSlider->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
