<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Title * </label>
    <div class="col-md-9">
        {!! Form::text('title', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter title', 'data-parsley-required'=>'true']) !!}
        <small class="f-s-12 text-grey-darker">(* Maximum 50 letter)</small>
         @if ($errors->has('title'))
        <span class="error-form">
            * {{ $errors->first('title') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Year * </label>
    <div class="col-md-9">
        {!! Form::text('year', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Year', 'data-parsley-required'=>'true']) !!}

         @if ($errors->has('year'))
        <span class="error-form">
            * {{ $errors->first('year') }}
        </span>
    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Logo/icon </label>
    <div class="col-md-9">
        {!! Form::text('logo', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter logo (Font Awasome)', 'data-parsley-required'=>'true']) !!}
         @if ($errors->has('logo'))
        <span class="error-form">
            * {{ $errors->first('logo') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Image </label>
    <div class="col-md-9">
        {!! Form::file('image', ['class' => 'form-control m-b-5','id'=>"imgInp"]) !!}
         @if ($errors->has('image'))
        <span class="error-form">
            * {{ $errors->first('image') }}
        </span>
    @endif
    @if(isset($timeline) && $timeline->image)
    <img id="blah" src="{{url($timeline->image)}}" alt="image" class="m-t-2" width="150"/>

    @else
    <img id="blah" src="#" alt="image" class="d-none m-t-2" width="150"/>

    @endif
    </div>
</div>

<div class="form-group row m-b-15">
    <label class="col-form-label col-md-3">Description</label>
    <div class="col-md-9">
        {!! Form::textarea('description', null, ['class' => 'form-control m-b-5','placeholder'=>'Enter Description', 'data-parsley-required'=>'true']) !!}
        @if ($errors->has('description'))
        <span class="error-form">
            * {{ $errors->first('description') }}
        </span>
    @endif
    </div>
</div>
<div class="form-group row m-b-10">
    <label class="col-md-3 col-form-label">Status</label>
    <div class="col-md-9 p-t-3">
        <div class="switcher">
            {!! Form::checkbox('status',1, (isset($timeline)) ? ($timeline->status == 1 ? true : false) : true, ['data-render'=>"switchery", 'data-theme'=>"blue"]) !!}
        </div>

    </div>
</div>
