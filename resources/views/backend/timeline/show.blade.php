
@extends('backend.layouts.master')

@section('content')


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Main Page Settings</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Timeline</a></li>
        <li class="breadcrumb-item active">Show</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Show Timeline <a class="btn btn-sm btn-success" href="{{route('timelines.index')}}"><i class="fa fa-arrow-left"></i> Back</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->

    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-inverse">

                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Show</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Title</label>
                    <div class="col-md-9">
                        {{$timeline->title}}
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Year</label>
                    <div class="col-md-9">
                        {{$timeline->year}}
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Image</label>
                    <div class="col-md-9">
                        @if(isset($timeline->image))<img src="{{url($timeline->image)}}" width="120"/> @endif
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Logo</label>
                    <div class="col-md-9">
                        {{$timeline->logo}}
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Description</label>
                    <div class="col-md-9">
                        {!!$timeline->description!!}
                    </div>
                </div>
                <div class="form-group row m-b-10">
                    <label class="col-md-3 col-form-label">Status</label>
                    {!!($timeline->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!}
                </div>


            </div>
            <!-- end panel-body -->
        </div>
        </div>

        </div>

            <div class="col-md-8 col-sm-8">
                <a class="btn btn-sm btn-success" href="{{route('timelines.index')}}"><i class="fa fa-arrow-left"></i> Back</a>
         </div>
    </div>

    <!-- end panel -->
</div>


@endsection
