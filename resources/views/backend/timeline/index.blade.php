
@extends('backend.layouts.master')

@section('content')


<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Main Page Settings</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Timeline</a></li>
        <li class="breadcrumb-item active">All</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Timelines <a class="btn btn-sm btn-success" href="{{route('timelines.create')}}"><i class="fa fa-plus"></i> Add Timeline</a><small></small></h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <h4 class="panel-title">Table</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>

            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th class="text-nowrap">Year</th>
                        <th class="text-nowrap">Name</th>
                        <th class="text-nowrap">Logo</th>
                        <th class="text-nowrap">Status</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($timelines as $timeline)
                    <tr class="odd gradeX">
                        <td width="1%" class="f-w-600 text-inverse">{{$loop->iteration}}</td>
                        <td>{{$timeline->year}} </td>
                        <td>{{$timeline->title}} </td>
                        <td>@if(isset($timeline->image))<img src="{{url($timeline->image) ?? "#"}}" width="120"/>@endif</td>
                        <td>{!!($timeline->status == 1) ? '<span class="label label-green">Active</span>' : '<span class="label label-danger">In-active</span>'!!} </td>
                        <td class="with-btn-group" nowrap="">
                            <div class="btn-group">
                                <a href="{{route('timelines.show',$timeline->id)}}" class="btn btn-info btn-sm width-90"><i class="fa fa-eye"></i> View</a>
                                <a href="#" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" style="">
                                    <a href="{{route('timelines.edit',$timeline->id)}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit</a>
                                    <form action="{{ route('timelines.destroy', [$timeline->id]) }}"
                                        method="DELETE" class="delete-data-ajax">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="dropdown-item" title="Delete"><i class="fa fa-trash"></i> Delete</button>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>


@endsection
