@extends('frontend.layouts.master')


@section('content')



  <!-- BEGIN #slider -->
  <div id="slider" class="section-container p-0 bg-black-darker">
    <!-- BEGIN carousel -->
    <div id="main-carousel" class="carousel slide" data-ride="carousel">
      <!-- BEGIN carousel-inner -->
      <div class="carousel-inner">

        @foreach ($sliders as $key => $slider)

        <div
        class="@if($key == 0 )carousel-item active @else carousel-item @endif"
        data-paroller="true"
        data-paroller-factor="0.3"
        data-paroller-factor-sm="0.01"
        data-paroller-factor-xs="0.01"
      >
      <a href="@if($slider->type == "Link")
        {{$slider->go_to}}
        @elseif($slider->type == "Slug")
        {{url($slider->go_to)}}
        @endif" target="@if($slider->same_page == "0") _blank @endif">
      <div style="
      background: url({{$slider->image}}) center 0 /
        cover no-repeat;
        min-height: 31.125rem;
    ">

      </div>
    </a>

      </div>
        @endforeach
        <!-- BEGIN item -->

        <!-- END item -->
        <!-- BEGIN item -->

      </div>
      <!-- END carousel-inner -->
      <a
        class="carousel-control-prev"
        href="#main-carousel"
        data-slide="prev"
      >
        <i class="fa fa-angle-left"></i>
      </a>
      <a
        class="carousel-control-next"
        href="#main-carousel"
        data-slide="next"
      >
        <i class="fa fa-angle-right"></i>
      </a>
    </div>
    <!-- END carousel -->
  </div>

  <section class="header3 cid-sdukFSYwTN" id="header03-2" style="margin-top: 0">
    @foreach ($abouts as $about)
    @if($loop->iteration % 2 == 0)
    <div class="container" style="margin-top: 80px">
      <div class="row justify-content-center mbr-white">

        <div class="col-12 col-md-12 col-lg">
          <div class="text-wrapper align-right">
            <h1
              class="mbr-section-right-title mbr-fonts-style mb-4 display-1"
            >
              <strong>{{$about->title}}</strong>
            </h1>
            <p class="mbr-text mbr-fonts-style display-7">
              {{$about->body}}
            </p>
            {{-- <div class="mbr-section-btn mt-4">
              <a
                class="btn btn-lg btn-white-outline display-7"
                href="404.html"
                data-aos="fade-up-left"
                >Get Started</a
              > --}}
            </div>
          </div>


        <div class="col-12 col-md-12 col-lg-5 md-pb">
            <div class="image-wrapper" data-aos="fade-left">
                <img
                class="w-100"
                src="{{asset($about->image)}}"
                alt="Mobirise"
                height="500"
                style="object-fit: cover"
                />
            </div>
            </div>
        </div>
      </div>
    </div>
    @else
    <div class="container"  style="margin-top: 80px">
        <div class="row justify-content-center mbr-white">
          <div class="col-12 col-md-12 col-lg-5 md-pb">
            <div class="image-wrapper" data-aos="fade-right">
              <img
                class="w-100"
                src="{{asset($about->image)}}"
                alt="Mobirise"
                height="500"
                style="object-fit: cover"
              />
            </div>
          </div>
          <div class="col-12 col-md-12 col-lg">
            <div class="text-wrapper align-left">
              <h1 class="mbr-section-title mbr-fonts-style mb-4 display-1">
                <strong>{{$about->title}}</strong>
              </h1>
              <p class="mbr-text mbr-fonts-style display-7">
                {{$about->body}}
              </p>
              {{-- <div class="mbr-section-btn mt-4">
                <a
                  class="btn btn-lg btn-white-outline display-7"
                  href="404.html"
                  data-aos="fade-up-right"
                  >Get Started</a
                >
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    @endif

    @endforeach
  </section>
  <!-- BEGIN #policy -->
  <div id="policy" class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
      <!-- BEGIN row -->
      <div class="row">
        <!-- BEGIN col-4 -->
        <div class="col-lg-4 col-md-4 mb-4 mb-md-0">
          <!-- BEGIN policy -->
          <div class="policy">
            <div class="policy-icon"><i class="fa fa-truck"></i></div>
            <div class="policy-info">
              <h4>Free Delivery</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </div>
          </div>
          <!-- END policy -->
        </div>
        <!-- END col-4 -->
        <!-- BEGIN col-4 -->
        <div class="col-lg-4 col-md-4 mb-4 mb-md-0">
          <!-- BEGIN policy -->
          <div class="policy">
            <div class="policy-icon"><i class="fa fa-umbrella"></i></div>
            <div class="policy-info">
              <h4>1 Year Warranty</h4>
              <p>
                Cras laoreet urna id dui malesuada gravida. <br />Duis a
                lobortis dui.
              </p>
            </div>
          </div>
          <!-- END policy -->
        </div>
        <!-- END col-4 -->
        <!-- BEGIN col-4 -->
        <div class="col-lg-4 col-md-4">
          <!-- BEGIN policy -->
          <div class="policy">
            <div class="policy-icon"><i class="fa fa-user-md"></i></div>
            <div class="policy-info">
              <h4>More than 100,000 Consumer</h4>
              <p>
                Fusce ut euismod orci. Morbi auctor, sapien non eleifend
                iaculis.
              </p>
            </div>
          </div>
          <!-- END policy -->
        </div>
        <!-- END col-4 -->
      </div>
      <!-- END row -->
    </div>
    <!-- END container -->
  </div>
  <!-- END #policy -->
  <section class="header3 timeline-header" id="header03-2">
    <div class="container" style="margin-top: 80px">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-center timeline-title">Milestone</h2>
        </div>
        <div class="col-md-12">
          <div class="main-timeline">
              @foreach ($timelines as $key => $timeline)
              <div class="timeline">
                <a href="" class="timeline-content">
                  <div class="timeline-year">
                    <span>{{$timeline->year}}</span>
                  </div>
                  <div class="inner-content" data-aos="{{($key % 2 == 0) ? 'fade-right' : 'fade-left' }}">
                    <h3 class="title">
                      {{$timeline->title}} @if(isset($timeline->logo))<i class="fa {{$timeline->logo}}"></i>@endif
                    </h3>
                    <p class="description">
                        @if(isset($timeline->image))
                        <img src="{{($timeline->image)}}" width="200" height="100%"/><br/>
                        @endif
                     {{$timeline->description}}
                    </p>
                  </div>
                </a>
              </div>
              @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="news-title">News</h2>
          </div>
        <div class="col-md-12">
            <div id="news-slider" class="owl-carousel">
                @foreach ($news as $new)
                <div class="post-slide">
                    <div class="post-img">
                        <img src="{{asset($new->image)}}" alt="">
                        <div class="over-layer">
                            <ul class="post-link">
                                <li><a href="#" class="fa fa-search"></a></li>
                                <li><a href="#" class="fa fa-link"></a></li>
                            </ul>
                        </div>
                        <div class="post-date">
                            <span class="date">{{date('d',strtotime($new->date))}}</span>
                            <span class="month">{{date('M',strtotime($new->date))}}</span>
                        </div>
                    </div>
                    <div class="post-content">
                        <h3 class="post-title">
                            <a href="#">{{$new->title}}</a>
                        </h3>
                        <p class="post-description">
                           {!! \Illuminate\Support\Str::limit($new->body, 150, $end='...') !!}
                        </p>
                        <a href="#" class="read-more">read more</a>
                    </div>
                </div>

                @endforeach

            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-12">
                <h2 class="news-title">Brands</h2>
              </div>
        <div class="col-md-12">
    <div class="owl_wrapper">
      <div class="owl_content_brands">
          @foreach ($brands as $brand)
            <a href="{{route('brand.page',$brand->slug)}}"><img src="{{url($brand->logo)}}" alt="" data-aos="fade-bottom"/></a>

          @endforeach
        {{-- <img src="{{asset('assets/img/brand/LG-New-Logo.png')}}" alt="" data-aos="fade-bottom"/>

        <img src="{{asset('assets/img/brand/Godrej-Logo.png')}}" alt="" data-aos="fade-bottom"/>
        <img src="{{asset('assets/img/brand/Elica-Colour-Logo-(PDF).png')}}" alt="" data-aos="fade-bottom"/>
        <img src="{{asset('assets/img/brand/KENT.png')}}" alt="" data-aos="fade-bottom"/>
        <img src="{{asset('assets/img/brand/Pigeon.png')}}" alt="" data-aos="fade-bottom"/>
        <img src="{{asset('assets/img/brand/Sensei.png')}}" alt="" data-aos="fade-bottom"/>
        <img src="{{asset('assets/img/brand/TCL.png')}}" alt="" data-aos="fade-bottom"/> --}}
      </div>
    </div>
  </div>
</div>

      </div>


<!-- BEGIN #subscribe -->
<div id="subscribe" class="section-container">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-6 -->
            <div class="col-md-6 mb-4 mb-md-0">
                <!-- BEGIN subscription -->
                <div class="subscription">
                    <div class="subscription-intro">
                        <h4>LET'S STAY IN TOUCH</h4>
                        <p>Get updates on sales specials and more</p>
                    </div>
                    <div class="subscription-form">
                        <form
                                name="subscription_form"
                                action="index.html"
                                method="POST"
                                >
                            <div class="input-group">
                                <input
                                        type="text"
                                        class="form-control"
                                        name="email"
                                        placeholder="Enter Email Address"
                                        />
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-inverse">
                                        <i class="fa fa-angle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END subscription -->
            </div>
            <!-- END col-6 -->
            <!-- BEGIN col-6 -->
            <div class="col-md-6">
                <!-- BEGIN social -->
                <div class="social">
                    <div class="social-intro">
                        <h4>FOLLOW US</h4>
                        <p>We want to hear from you!</p>
                    </div>
                    <div class="social-list">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-dribbble"></i></a>
                        <a href="#"><i class="fab fa-google-plus"></i></a>
                    </div>
                </div>
                <!-- END social -->
            </div>
            <!-- END col-6 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #subscribe -->

<!-- BEGIN #footer -->
<div id="footer" class="footer">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-3 -->
            <div class="col-lg-3">
                <h4 class="footer-header">ABOUT US</h4>
              <span class="brand-text">
                <img
                        src="{{asset('assets/img/brand/CG-Electronics-New.png')}}"
                        width="150"
                        class="mb-3"
                        style="filter: brightness(0) invert(1);"
                        />
                <!-- <span class="text-primary">Color</span>Admin
							<small>e-commerce frontend theme</small> -->
              </span>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec
                    tristique dolor, ac efficitur velit. Nulla lobortis tempus
                    convallis. Nulla aliquam lectus eu porta pulvinar. Mauris semper
                    justo erat.
                </p>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-lg-3">
                <h4 class="footer-header">DISTRIBUTION REACH & NETWORK</h4>
                <ul class="fa-ul mb-lg-4 mb-0">
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">CG</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">LG</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">TCL</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Sensai</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Elica</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Godrej</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Pigeon</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Kent</a>
                    </li>
                </ul>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-lg-3">
                <h4 class="footer-header">ASSOCIATED BRANDS</h4>
                <ul class="fa-ul mb-lg-4 mb-0">
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">CG</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">LG</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">TCL</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Sensai</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Elica</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Godrej</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Pigeon</a>
                    </li>
                    <li>
                        <i class="fa fa-li fa-angle-right"></i> <a href="#">Kent</a>
                    </li>
                </ul>
            </div>
            <!-- END col-3 -->

            <!-- BEGIN col-3 -->
            <div class="col-lg-3">
                <h4 class="footer-header">OTHER</h4>
                <ul class="fa-ul other-ul mb-lg-4 mb-0">
                    <li>
                        <a href="#">Blog</a>
                    </li>
                    <li>
                        <a href="#">Carrer</a>
                    </li>
                    <li>
                        <a href="#">Newsroom</a>
                    </li>
                    <li>
                        <a href="#">CSR</a>
                    </li>
                    <li>
                        <a href="#">Support</a>
                    </li>
                    <li>
                        <a href="#">Offering</a>
                    </li>
                    <li>
                        <a href="#">Where to Buy</a>
                    </li>
                    <li>
                        <a href="#">Contact us</a>
                    </li>
                </ul>
            </div>
            <!-- END col-3 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #footer -->


@endsection
