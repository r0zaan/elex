
@extends('frontend.layouts.master')


@section('content')
<div id="page-header" class="section-container page-header-container bg-black">
    <!-- BEGIN page-header-cover -->
    <div class="page-header-cover">
        <img src="../assets/img/cover/cover-15.jpg" alt="" />
    </div>
    <!-- END page-header-cover -->
    <!-- BEGIN container -->
    <div class="container">
        <h1 class="page-header"><b>{{$page->title}}</b></h1>
    </div>
    <!-- END container -->
</div>
<!-- BEGIN #page-header -->

<!-- BEGIN #product -->
<div id="product" class="section-container p-t-20">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN breadcrumb -->
        <ul class="breadcrumb m-b-10 f-s-12">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Page</a></li>
            <li class="breadcrumb-item active">{{$page->title}}</li>
        </ul>
        <!-- END breadcrumb -->
        <!-- BEGIN row -->
        <div class="row row-space-30">
            <!-- BEGIN col-8 -->
            <div class="col-md-12">
                {!!$page->body!!}
            </div>
            <!-- END col-8 -->

            <!-- END col-4 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END row -->
</div>


@endsection
