<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>CG electronics</title>
    <meta
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
      name="viewport"
    />
    <meta content="" name="description" />
    <meta content="" name="author" />
    @if(isset($seletedBrand))
    <link
      rel="icon"
      type="image/png"
      href="{{url($seletedBrand->logo)}}"
    />

    @else
    <link
    rel="icon"
    type="image/png"
    href="{{asset('assets/img/icon/cg-inspiring-life-final-vector-logo.png')}}"
  />
  @endif
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
    <link href="{{asset('assets/css/e-commerce/app.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">

    <link href="{{asset('assets/css/custom/custom.css')}}" rel="stylesheet" />

      @if(isset($seletedBrand))

          <style>
              body{
                  background: white!important;
              }
          </style>

      @endif

    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <!-- ================== END BASE CSS STYLE ================== -->
  </head>
  <body></body>
    <!-- BEGIN #page-container -->
    <div id="page-container" class="fade show">

 <!-- BEGIN #top-nav -->
 <div id="top-nav" class="top-nav" @if(!empty($seletedBrand)) style="background: {{$seletedBrand->brand_color_code}}!important;" @endif>
    <!-- BEGIN container -->
    <div class="container">
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <!-- <li class="dropdown dropdown-hover">
                        <a href="#" data-toggle="dropdown"><img src="{{asset('assets/img/flag/flag-english.png')}}" class="flag-img" alt="" /> English <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="dropdown-item"><img src="{{asset('assets/img/flag/flag-english.png')}}" class="flag-img" alt="" /> English</a></li>
                            <li><a href="#" class="dropdown-item"><img src="{{asset('assets/img/flag/flag-german.png')}}" class="flag-img" alt="" /> German</a></li>
                            <li><a href="#" class="dropdown-item"><img src="{{asset('assets/img/flag/flag-spanish.png')}}" class="flag-img" alt="" /> Spanish</a></li>
                            <li><a href="#" class="dropdown-item"><img src="{{asset('assets/img/flag/flag-french.png')}}" class="flag-img" alt="" /> French</a></li>
                            <li><a href="#" class="dropdown-item"><img src="{{asset('assets/img/flag/flag-chinese.png')}}" class="flag-img" alt="" /> Chinese</a></li>
                        </ul>
                    </li> -->
          <li><a href="#">Apply for Dealership</a></li>
          <li><a href="#">Support</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">CSR</a></li>
          <li><a href="#">Career</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">News room</a></li>
          <li>
            <a href="#"><i class="fab fa-facebook-f f-s-14"></i></a>
          </li>
          <li>
            <a href="#"><i class="fab fa-twitter f-s-14"></i></a>
          </li>
          <li>
            <a href="#"><i class="fab fa-instagram f-s-14"></i></a>
          </li>
          <li>
            <a href="#"><i class="fab fa-dribbble f-s-14"></i></a>
          </li>
          <li>
            <a href="#"><i class="fab fa-google f-s-14"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <!-- END container -->
  </div>

  <!-- BEGIN #header -->
  <div id="header" class="header " data-fixed-top="true">
    <!-- BEGIN container -->
    <div class="container">
      <!-- BEGIN header-container -->
      <div class="header-container">
        <!-- BEGIN navbar-toggle -->
        <button
          type="button"
          class="navbar-toggle collapsed"
          data-toggle="collapse"
          data-target="#navbar-collapse"
        >
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- END navbar-toggle -->
        <!-- BEGIN header-logo -->
        <div class="header-logo">
          <a href="{{route('home')}}">
            <!-- <span class="brand-logo">

                        </span> -->
            <span class="brand-text">
                @if(isset($seletedBrand))
                <img
                src="{{url($seletedBrand->logo)}}"
                height="100%"
                width="150"
                style="padding: 10px;
                 max-height: 55rem;
                 max-width: 15rem;"
              />
                @else
              <img
                src="{{url('assets/img/brand/CG-Electronics-New.png')}}"
                height="100%"
                width="150"
              />
              @endif
              <!-- <span class="text-primary">Color</span>Admin
                            <small>e-commerce frontend theme</small> -->
            </span>
          </a>
        </div>
        <!-- END header-logo -->
        <!-- BEGIN header-nav -->
        <div class="header-nav">
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav">
                @foreach(\App\Models\MenuBuilder::whereNull('parent_id')->orderBy('sort_number','ASC')->Active()->get() as $menu)
                @if($menu->type == "Link" || $menu->type == "Slug")
                    <li class="active"><a href="{{url($menu->go_to)}}">{{$menu->name}}</a></li>
                    @elseif($menu->type == "Dropdown")
                    <li class="dropdown dropdown-hover">
                        <a href="#" data-toggle="dropdown">
                            {{$menu->name}}
                          <b class="caret"></b>
                          <span class="arrow top"></span>
                        </a>
                        <div class="dropdown-menu">
                        @foreach($menu->childMenus()->orderBy('sort_number','ASC')->Active()->get() as $subMenu)
                                <a class="dropdown-item" href="{{url($subMenu->go_to)}}">{{$subMenu->name}}</a>
                          @endforeach
                        </div>
                      </li>
                      @elseif($menu->type == "Brand")
                      <li class="dropdown dropdown-full-width dropdown-hover">
                        <a href="#" data-toggle="dropdown">
                          Brand
                          <b class="caret"></b>
                          <span class="arrow top"></span>
                        </a>
                        <!-- BEGIN dropdown-menu -->
                        <div class="dropdown-menu p-0">
                          <!-- BEGIN dropdown-menu-container -->
                          <div class="dropdown-menu-container">
                            <!-- BEGIN dropdown-menu-sidebar -->
                            <!-- <div class="dropdown-menu-sidebar">
                                                    <h4 class="title">Shop By Categories</h4>
                                                    <ul class="dropdown-menu-list">
                                                        <li><a href="404.html">Mobile Phone</a></li>
                                                        <li><a href="404.html">Tablet</a></li>
                                                        <li><a href="404.html">Laptop</a></li>
                                                        <li><a href="404.html">Desktop</a></li>
                                                        <li><a href="404.html">TV</a></li>
                                                        <li><a href="404.html">Speaker</a></li>
                                                        <li><a href="404.html">Gadget</a></li>
                                                    </ul>
                                                </div> -->
                            <!-- END dropdown-menu-sidebar -->
                            <!-- BEGIN dropdown-menu-content -->
                            <div class="dropdown-menu-content">
                              <!-- <h4 class="title">Shop By Popular Phone</h4>
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <ul class="dropdown-menu-list">
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> iPhone 7</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> iPhone 6s</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> iPhone 6</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> iPhone 5s</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> iPhone 5</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <ul class="dropdown-menu-list">
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Galaxy S7</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Galaxy A9</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Galaxy J3</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Galaxy Note 5</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Galaxy S7</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <ul class="dropdown-menu-list">
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Lumia 730</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Lumia 735</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Lumia 830</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Lumia 820</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Lumia Icon</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <ul class="dropdown-menu-list">
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Xperia X</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Xperia Z5</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Xperia M5</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Xperia C5</a></li>
                                                                <li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i> Xperia C4</a></li>
                                                            </ul>
                                                        </div>
                                                    </div> -->
                              <h4 class="title">{{$menu->name}}</h4>
                              <ul class="dropdown-brand-list m-b-0">
                                @foreach($brands as $brand)
                                <li>
                                  <a href="{{route('brand.page', [$brand->slug])}}"
                                    ><img
                                      src="{{asset($brand->logo)}}"
                                      alt=""
                                  /></a>
                                </li>
                                @endforeach
                                <li>
                                  <a href="404.html"
                                    ><img
                                      src="{{asset('assets/img/brand/Godrej-Logo.png')}}"
                                      alt=""
                                  /></a>
                                </li>
                                <li>
                                  <a href="404.html"
                                    ><img
                                      src="{{asset('assets/img/brand/Elica-Colour-Logo-(PDF).png')}}"
                                      alt=""
                                  /></a>
                                </li>

                                <li>
                                  <a href="404.html"
                                    ><img
                                      src="{{asset('assets/img/brand/Pigeon.png')}}"
                                      alt=""
                                  /></a>
                                </li>
                                <li>
                                  <a href="404.html"
                                    ><img
                                      src="{{asset('assets/img/brand/Sensei.png')}}"
                                      alt=""
                                  /></a>
                                </li>
                                {{--<li>--}}
                                  {{--<a href="404.html"--}}
                                    {{--><img src="{{asset('assets/img/brand/TCL.png')}}" alt=""--}}
                                  {{--/></a>--}}
                                {{--</li>--}}
                              </ul>
                            </div>
                            <!-- END dropdown-menu-content -->
                          </div>
                          <!-- END dropdown-menu-container -->
                        </div>
                        <!-- END dropdown-menu -->
                      </li>
                      @elseif($menu->type == "Large Dropdown")
                      <li class="dropdown dropdown-full-width dropdown-hover">
                        <a href="#" data-toggle="dropdown">
                            {{$menu->name}}
                            <b class="caret"></b>
                            <span class="arrow top"></span>
                        </a>
                        <!-- BEGIN dropdown-menu -->
                        <div class="dropdown-menu p-0">
                            <!-- BEGIN dropdown-menu-container -->
                            <div class="dropdown-menu-container">
                                <!-- BEGIN dropdown-menu-sidebar -->
                                <!-- END dropdown-menu-sidebar -->
                                <!-- BEGIN dropdown-menu-content -->
                                <div class="dropdown-menu-content">
                                    {{-- <h4 class="title">{{$menu->name}}</h4> --}}
                                    @foreach($menu->childMenus()->orderBy('sort_number','ASC')->Active()->get()->chunk(4) as $chunkMenu)
                                        <div class="row">
                                            @foreach($chunkMenu as $subMenu)

                                                <div class="col-lg-3">
                                                    <h4 class="title">{{$subMenu->name ?? null}}</h4>
                                                    <ul class="dropdown-menu-list">
                                                        @foreach($subMenu->childMenus()->orderBy('sort_number','ASC')->Active()->get() as $subTypeMenu)
                                                        <li><a href="{{url($subTypeMenu->go_to)}}">{{$subTypeMenu->name}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endforeach
                                        </div>
                                     @endforeach

                                    {{-- <div class="row">
                                        <div class="col-lg-3">
                                            <h4 class="title">Shop By Categories</h4>
                                            <ul class="dropdown-menu-list">
                                                <li><a href="product.html">Mobile Phone</a></li>
                                                <li><a href="product.html">Tablet</a></li>
                                                <li><a href="product.html">Laptop</a></li>
                                                <li><a href="product.html">Desktop</a></li>
                                                <li><a href="product.html">TV</a></li>
                                                <li><a href="product.html">Speaker</a></li>
                                                <li><a href="product.html">Gadget</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3">
                                            <h4 class="title">Shop By Categories</h4>
                                            <ul class="dropdown-menu-list">
                                                <li><a href="product.html">Mobile Phone</a></li>
                                                <li><a href="product.html">Tablet</a></li>
                                                <li><a href="product.html">Laptop</a></li>
                                                <li><a href="product.html">Desktop</a></li>
                                                <li><a href="product.html">TV</a></li>
                                                <li><a href="product.html">Speaker</a></li>
                                                <li><a href="product.html">Gadget</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3">
                                            <h4 class="title">Shop By Categories</h4>
                                            <ul class="dropdown-menu-list">
                                                <li><a href="product.html">Mobile Phone</a></li>
                                                <li><a href="product.html">Tablet</a></li>
                                                <li><a href="product.html">Laptop</a></li>
                                                <li><a href="product.html">Desktop</a></li>
                                                <li><a href="product.html">TV</a></li>
                                                <li><a href="product.html">Speaker</a></li>
                                                <li><a href="product.html">Gadget</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3">
                                            <h4 class="title">Shop By Categories</h4>
                                            <ul class="dropdown-menu-list">
                                                <li><a href="product.html">Mobile Phone</a></li>
                                                <li><a href="product.html">Tablet</a></li>
                                                <li><a href="product.html">Laptop</a></li>
                                                <li><a href="product.html">Desktop</a></li>
                                                <li><a href="product.html">TV</a></li>
                                                <li><a href="product.html">Speaker</a></li>
                                                <li><a href="product.html">Gadget</a></li>
                                            </ul>
                                        </div>
                                    </div> --}}
                                </div>
                                <!-- END dropdown-menu-content -->
                            </div>
                            <!-- END dropdown-menu-container -->
                        </div>
                        <!-- END dropdown-menu -->
                    </li>
                @endif
                @endforeach

              <!-- <li class="dropdown dropdown-hover">
                <a href="#" data-toggle="dropdown">
                  Distribution Reach
                  <b class="caret"></b>
                  <span class="arrow top"></span>
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="404.html">Mobile Phone</a>
                  <a class="dropdown-item" href="404.html">Tablet</a>
                  <a class="dropdown-item" href="404.html">TV</a>
                  <a class="dropdown-item" href="404.html">Desktop</a>
                  <a class="dropdown-item" href="404.html">Laptop</a>
                  <a class="dropdown-item" href="404.html">Speaker</a>
                  <a class="dropdown-item" href="404.html">Gadget</a>
                </div>
              </li> -->
              {{-- <li><a href="404.html">Distribution Reach</a></li>
              <li><a href="404.html">Offering</a></li>

              <li><a href="404.html">Support</a></li>
              <li><a href="404.html">Contact</a></li> --}}
              <!-- <li class="dropdown dropdown-hover">
                                <a href="#" data-toggle="dropdown">
                                    <i class="fa fa-search search-btn"></i>
                                    <span class="arrow top"></span>
                                </a>
                                <div class="dropdown-menu p-15">
                                    <form action="search_results.html" method="POST" name="search_form">
                                        <div class="input-group">
                                            <input type="text" placeholder="Search" class="form-control bg-silver-lighter" />
                                            <div class="input-group-append">
                                                <button class="btn btn-inverse" type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li> -->
            </ul>
          </div>
        </div>
        <!-- END header-nav -->
        <!-- BEGIN header-nav -->
        <div class="header-nav">
          <ul class="nav pull-right">
            <li class="brand-text">
              <img
                src="{{asset('assets/img/brand/ChaudharyGroup.png')}}"
                alt="ChaudharyGroup"
                height="100%"
                width="150"
              />
            </li>
          </ul>
        </div>
        <!-- END header-nav -->
      </div>
      <!-- END header-container -->
    </div>
    <!-- END container -->
  </div>
  <!-- END #header -->
        @yield('content')




      <!-- END #slider -->

      <div id="footer-copyright" class="footer-copyright">
        <!-- BEGIN container -->
        <div class="container">
          <!-- BEGIN row -->
          <div class="row">
            <!-- <div class="col-lg-12">
						<h4 class="text-center">Contact Us</h4>
					</div> -->
            <!-- BEGIN col-3 -->
            <div class="col-lg-4">
              <h4 class="footer-header"><i class="fa fa-users"></i> Our Contact</h4>
              <address class="mb-lg-4 mb-0 address-footer-ml">
                <strong>CG Digital Park<br />Satungal, Kathmandu, Nepal </strong
                ><br />
                <abbr title="Phone">Phone:</abbr> 166-00-100-211/ 01-518824<br />
                <!-- <abbr title="Fax">Fax:</abbr> (123) 456-7891<br /> -->
                <abbr title="Email">Email:</abbr>
                <a href="mailto:service.support@chaudharygroup.com"
                  >service.support@chaudharygroup.com</a
                ><br />
                <!-- <abbr title="Skype">Skype:</abbr> <a href="skype:myshop">myshop</a> -->
              </address>
            </div>

            <div class="col-lg-4"><h4 class="footer-header"><i class="fa fa-tools"></i> For Service only</h4>
				<address class="mb-lg-4 mb-0 address-footer-ml">
				  <strong>CG Digital Park<br />Satungal, Kathmandu, Nepal </strong
				  ><br />
				  <abbr title="Phone">Phone:</abbr> 166-00-100-211/ 01-518824<br />
				  <!-- <abbr title="Fax">Fax:</abbr> (123) 456-7891<br /> -->
				  <abbr title="Email">Email:</abbr>
				  <a href="mailto:service.support@chaudharygroup.com"
					>service.support@chaudharygroup.com</a
				  ><br /></div>
            <div class="col-lg-4"><h4 class="footer-header"><i class="fa fa-briefcase"></i> For Business only</h4>
				<address class="mb-lg-4 mb-0 address-footer-ml">
				  <strong>CG Digital Park<br />Satungal, Kathmandu, Nepal </strong
				  ><br />
				  <abbr title="Phone">Phone:</abbr> 01-5108120/ 9851211096<br />
				  <!-- <abbr title="Fax">Fax:</abbr> (123) 456-7891<br /> -->
				  <abbr title="Email">Email:</abbr>
				  <a href="mailto:reshma.sunam@chaudharygroup.com"
					>reshma.sunam@chaudharygroup.com</a
				  ><br /></div>
          </div>
        </div>
        <!-- END container -->
      </div>
      <!-- BEGIN #footer-copyright -->
      <div id="footer-copyright" class="footer-copyright">
        <!-- BEGIN container -->
        <div class="container">
          <div class="payment-method">Privacy Policy</div>
          <div class="copyright">
            CG Electronics &copy; All rights reserved.
          </div>
        </div>
        <!-- END container -->
	  </div>
	<a id="back-to-top" href="#" class="btn btn-light btn-sm back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>

      <!-- END #footer-copyright -->
	</div>
    <!-- END #page-container -->

    <!-- begin theme-panel -->
    <!-- <div class="theme-panel">
		<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="{{asset('assets/css/e-commerce/theme/red.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="{{asset('assets/css/e-commerce/theme/pink.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="{{asset('assets/css/e-commerce/theme/orange.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="{{asset('assets/css/e-commerce/theme/yellow.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="{{asset('assets/css/e-commerce/theme/lime.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="{{asset('assets/css/e-commerce/theme/green.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green" data-original-title="" title="">&nbsp;</a></li>
				<li class="active"><a href="javascript:;" class="bg-teal" data-theme-file="" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="{{asset('assets/css/e-commerce/theme/aqua.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="{{asset('assets/css/e-commerce/theme/blue.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="{{asset('assets/css/e-commerce/theme/purple.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="{{asset('assets/css/e-commerce/theme/indigo.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="{{asset('assets/css/e-commerce/theme/black.min.css')}}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black" data-original-title="" title="">&nbsp;</a></li>
			</ul>
		</div>
	</div> -->
    <!-- end theme-panel -->

    <!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('assets/js/e-commerce/app.min.js')}}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

	<script>
		$(document).ready(function(){
			$(document).ready(function() {
    $("#news-slider").owlCarousel({
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        autoPlay:true
    });



});
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
});
$(document).ready(function(){
  $(".owl_content_brands").owlCarousel({
    itemsScaleUp : true,
    slideSpeed: 500,
    autoPlay: 5000,
  });

  var owl = $(".owl_content").data('owlCarousel');
  $('.owl_wrapper .next').click(function(){owl.next();});
  $('.owl_wrapper .prev').click(function(){owl.prev();});
});
	</script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <script>
      AOS.init();
    </script>
    <!-- ================== END BASE JS ================== -->
  </body>
</html>
