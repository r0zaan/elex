
@extends('frontend.layouts.brand')


@section('content')
<!-- BEGIN #page-header -->
	<!-- BEGIN #slider -->
    <div id="slider" class="section-container p-0 bg-black-darker">
        <!-- BEGIN carousel -->
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <!-- BEGIN carousel-inner -->
            <div class="carousel-inner">
                <div class="carousel-item active" data-paroller="true" data-paroller-factor="-0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url({{url('colorAdmin/image/lg-banner-2.webp')}}) center 0 / cover no-repeat;">

                </div>
                <!-- BEGIN item -->
                <div class="carousel-item" data-paroller="true" data-paroller-factor="0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url({{url('colorAdmin/image/lg-banner.webp')}}) center 0 / cover no-repeat;">

                </div>
                <!-- END item -->
                <!-- BEGIN item -->

                <!-- END item -->
                <!-- BEGIN item -->
                <div class="carousel-item" data-paroller="true" data-paroller-factor="-0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url({{url('colorAdmin/image/lg-banner-3.webp')}}) center 0 / cover no-repeat;">

                </div>
                <!-- END item -->
            </div>
            <!-- END carousel-inner -->
            <a class="carousel-control-prev" href="#main-carousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#main-carousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
            </a>
        </div>
        <!-- END carousel -->
    </div>
    <!-- END #slider -->

    <!-- BEGIN #promotions -->
    <div id="promotions" class="section-container bg-white" style="background: {{$seletedBrand->brand_color_code}}!important;">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="#" class="pull-right">SHOW ALL</a>
                Exclusive promotions
                {{-- <small>from 25 September 2016 - 1 January 2017</small> --}}
            </h4>
            <!-- END section-title -->
            <!-- BEGIN row -->
            <div class="row row-space-10">
                <!-- BEGIN col-6 -->
                <div class="col-lg-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion promotion-lg bg-black-darker" style="    background-color: #848c92!important;">
                        <div class="promotion-image text-right promotion-image-overflow-bottom">
                            <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse">
                            <h4 class="promotion-title">LG 75” UHD 4K TV​​</h4>
                            <div class="promotion-price"><small>from</small> NRP 4,27,690</div>
                            <p class="promotion-desc">A big step for small.<br />A beloved design. Now with more to love.</p>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-6 -->
                <!-- BEGIN col-3 -->
                <div class="col-lg-3 col-md-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-blue" style="background-color: #ca4863!important;">
                        <div class="promotion-image promotion-image-overflow-bottom promotion-image-overflow-top" >
                            <img src="{{url('colorAdmin/image/lg-product-3.png')}}" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse text-right">
                            <h4 class="promotion-title">LG Refrigerator 471 Ltr</h4>
                            <div class="promotion-price"><small>from</small> NPR 86,790</div>
                            <p class="promotion-desc">You. At a glance.</p>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-silver-lighter">
                        <div class="promotion-image text-center promotion-image-overflow-bottom">
                            <img src="{{url('colorAdmin/image/lg-product-5.png')}}" alt="" />
                        </div>
                        <div class="promotion-caption text-center">
                            <h4 class="promotion-title">1000W Home Theater System</h4>
                            <div class="promotion-price"><small></small> NPR 45,790</div>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-lg-3 col-md-6">
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-silver-lighter">
                        <div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom text-right">
                            <img src="{{url('colorAdmin/image/lg-product-6.webp')}}" alt="" />
                        </div>
                        <div class="promotion-caption text-center">
                            <h4 class="promotion-title">LG Washing Machine 7.0 KG</h4>
                            <div class="promotion-price"> NPR 65,990</div>
                            <p class="promotion-desc">Redesigned. Remarkable.</p>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                    <!-- BEGIN promotion -->
                    <div class="promotion bg-black">
                        <div class="promotion-image text-right">
                            <img src="{{url('colorAdmin/image/lg-product-4.png')}}" alt="" />
                        </div>
                        <div class="promotion-caption promotion-caption-inverse">
                            <h4 class="promotion-title">49" UHD Smart LED TV</h4>
                            <div class="promotion-price"><small>from</small> NPR 95,790</div>
                            <p class="promotion-desc">Built for creativity on an epic scale.</p>
                            <a href="#" class="promotion-btn">View More</a>
                        </div>
                    </div>
                    <!-- END promotion -->
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #promotions -->

    <!-- BEGIN #trending-items -->
    <div id="trending-items" class="section-container">
        <!-- BEGIN container -->
        <div class="container-fluid">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="#" class="pull-right m-l-5"><i class="fa fa-angle-right f-s-18"></i></a>
                <a href="#" class="pull-right"><i class="fa fa-angle-left f-s-18"></i></a>
                Trending Items
                <small>Shop and get your favourite items at amazing prices!</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN row -->
            <div class="row row-space-10">

                     <!-- BEGIN col-2 -->
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr6.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 20% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG SM86 65 (165.1cm) 4K NanoCell TV<br /></a>
                            </h4>
                            <p class="item-desc">65SM8600PTA</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.206990</div>
                            <div class="item-discount-price">Rs.256990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>


                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr5.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 10% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG UN71 55 (139.7cm) 4K Smart UHD TV<br /></a>
                            </h4>
                            <p class="item-desc">55UN7190PTA</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.79990</div>
                            <div class="item-discount-price">Rs.84990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>


                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr4.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 15% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG Nano99 75 (190.5cm) 8K NanoCell TV<br /></a>
                            </h4>
                            <p class="item-desc">75NANO95TNA</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.1099990</div>
                            <div class="item-discount-price">Rs.1129990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>


                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr1.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 27% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG UM72 65 (165.1cm) 4K Smart UHD TV<br /></a>
                            </h4>
                            <p class="item-desc">65UM7290PTD</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.101990</div>
                            <div class="item-discount-price">Rs.139990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>

                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr2.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 15% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG UN73 70 (177.8cm) 4K Smart UHD TV<br /></a>
                            </h4>
                            <p class="item-desc">70UN7300PTC</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.169990</div>
                            <div class="item-discount-price">Rs.189990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>

                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr3.png')}}" alt="" />
                            <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 10% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG Nano95 75 (190.5cm) 8K NanoCell TV<br /></a>
                            </h4>
                            <p class="item-desc">75NANO95TNA</p>
                            <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.189990</div>
                            <div class="item-discount-price">Rs.199990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <!-- END col-2 -->


            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #trending-items -->

    <!-- BEGIN #mobile-list -->
    <div id="mobile-list" class="section-container p-t-0">
        <!-- BEGIN container -->
        <div class="container-fluid">
            <!-- BEGIN section-title -->
            <h4 class="section-title clearfix">
                <a href="#" class="pull-right">SHOW ALL</a>
                TV & HOME AUDIO SYSTEM
                <small>Shop and get your favourite phone at amazing prices!</small>
            </h4>
            <!-- END section-title -->
            <!-- BEGIN category-container -->
            <div class="category-container">
                <!-- BEGIN category-sidebar -->
                <div class="category-sidebar">
                    <ul class="category-list">
                        <li class="list-header">Top Categories</li>
                        <li><a href="#">LED TV</a></li>
                        <li><a href="#">AUDIO STUDIO</a></li>

                    </ul>
                </div>
                <!-- END category-sidebar -->
                <!-- BEGIN category-detail -->
                <div class="category-detail">
                    <!-- BEGIN category-item -->
                    <a href="#" class="category-item full">
                        <div class="item">
                            <div class="item-cover">
                                <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                            </div>
                            <div class="item-info bottom">
                                <h4 class="item-title">LG 75” UHD 4K TV</h4>
                                <p class="item-desc">Redefine what a TV can do</p>
                                <div class="item-price">NRP 4,27,690</div>
                            </div>
                        </div>
                    </a>
                    <!-- END category-item -->
                    <!-- BEGIN category-item -->
                    <div class="category-item list">
                        <!-- BEGIN item-row -->
                        <div class="item-row">
                            <!-- BEGIN item -->
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 40% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product_detail.html">LG 75” UHD 4K TV</a>
                                    </h4>
                                    <p class="item-desc">Redefine what a TV can do.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 7,16,490</div>
                                    <div class="item-discount-price">NRP 4,27,690</div>
                                </div>
                            </div>
                            <!-- END item -->
                            <!-- BEGIN item -->
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-4.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 26% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product.html">49" UHD Smart LED TV</a>
                                    </h4>
                                    <p class="item-desc">Super. Computer. Now in two sizes.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 95,790</div>
                                    <div class="item-discount-price">NPR 1,29,990</div>
                                </div>
                            </div>
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 40% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product_detail.html">LG 75” UHD 4K TV</a>
                                    </h4>
                                    <p class="item-desc">Redefine what a TV can do.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 7,16,490</div>
                                    <div class="item-discount-price">NRP 4,27,690</div>
                                </div>
                            </div>
                        </div>
                        <div class="item-row">
                            <!-- BEGIN item -->
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 40% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product_detail.html">LG 75” UHD 4K TV</a>
                                    </h4>
                                    <p class="item-desc">Redefine what a TV can do.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 7,16,490</div>
                                    <div class="item-discount-price">NRP 4,27,690</div>
                                </div>
                            </div>
                            <!-- END item -->
                            <!-- BEGIN item -->
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-4.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 26% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product.html">49" UHD Smart LED TV</a>
                                    </h4>
                                    <p class="item-desc">Super. Computer. Now in two sizes.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 95,790</div>
                                    <div class="item-discount-price">NPR 1,29,990</div>
                                </div>
                            </div>
                            <div class="item item-thumbnail">
                                <a href="product_detail.html" class="item-image">
                                    <img src="{{url('colorAdmin/image/lg-product-2-png.png')}}" alt="" />
                                    <div class="discount"style="background: {{$seletedBrand->brand_color_code}}!important;"> 40% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product_detail.html">LG 75” UHD 4K TV</a>
                                    </h4>
                                    <p class="item-desc">Redefine what a TV can do.</p>
                                    <div class="item-price" style="color: {{$seletedBrand->brand_color_code}}!important;">NPR 7,16,490</div>
                                    <div class="item-discount-price">NRP 4,27,690</div>
                                </div>
                            </div>
                        </div>
                        <!-- END item-row -->

                    </div>
                    <!-- END category-item -->
                </div>
                <!-- END category-detail -->
            </div>
            <!-- END category-container -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #mobile-list -->


<!-- BEGIN #subscribe -->
<div id="subscribe" class="section-container">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-6 -->
            <div class="col-md-6 mb-4 mb-md-0">
                <!-- BEGIN subscription -->
                <div class="subscription">
                    <div class="subscription-intro">
                        <h4>LET'S STAY IN TOUCH</h4>
                        <p>Get updates on sales specials and more</p>
                    </div>
                    <div class="subscription-form">
                        <form
                                name="subscription_form"
                                action="index.html"
                                method="POST"
                                >
                            <div class="input-group">
                                <input
                                        type="text"
                                        class="form-control"
                                        name="email"
                                        placeholder="Enter Email Address"
                                        />
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-inverse">
                                        <i class="fa fa-angle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END subscription -->
            </div>
            <!-- END col-6 -->
            <!-- BEGIN col-6 -->
            <div class="col-md-6">
                <!-- BEGIN social -->
                <div class="social">
                    <div class="social-intro">
                        <h4>FOLLOW US</h4>
                        <p>We want to hear from you!</p>
                    </div>
                    <div class="social-list">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-dribbble"></i></a>
                        <a href="#"><i class="fab fa-google-plus"></i></a>
                    </div>
                </div>
                <!-- END social -->
            </div>
            <!-- END col-6 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #subscribe -->




@endsection
