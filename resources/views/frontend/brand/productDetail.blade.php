
@extends('frontend.layouts.brand')


@section('content')

    <!-- BEGIN #product -->
    <div id="product" class="section-container p-t-20">
        <!-- BEGIN container -->
        <div class="container-fluid">
            <!-- BEGIN breadcrumb -->
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">TV/AUDIO/VIDEO</a></li>
                <li class="breadcrumb-item"><a href="#">TVs</a></li>
                <li class="breadcrumb-item active">65UN7300PTC</li>
            </ul>
            <!-- END breadcrumb -->
            <!-- BEGIN product -->
            <div class="product">
                <!-- BEGIN product-detail -->
                <div class="product-detail">
                    <!-- BEGIN product-image -->
                    <div class="product-image">
                        <!-- BEGIN product-thumbnail -->
                        <div class="product-thumbnail">
                            <ul class="product-thumbnail-list">
                                <li class="active"><a href="#" data-click="show-main-image" data-url="{{asset('assets/img/product/lg-product1.webp')}}"><img src="{{asset('assets/img/product/lg-product1.webp')}}" alt="" /></a></li>
                                <li><a href="#" data-click="show-main-image" data-url="{{asset('assets/img/product/lg-product2.webp')}}"><img src="{{asset('assets/img/product/lg-product2.webp')}}" alt="" /></a></li>
                                <li><a href="#" data-click="show-main-image" data-url="{{asset('assets/img/product/lg-product3.webp')}}"><img src="{{asset('assets/img/product/lg-product3.webp')}}" alt="" /></a></li>
                                <li><a href="#" data-click="show-main-image" data-url="{{asset('assets/img/product/lg-product4.webp')}}"><img src="{{asset('assets/img/product/lg-product4.webp')}}" alt="" /></a></li>
                            </ul>
                        </div>
                        <!-- END product-thumbnail -->
                        <!-- BEGIN product-main-image -->
                        <div class="product-main-image" data-id="main-image">
                            <img src="{{asset('assets/img/product/lg-product1.webp')}}" alt="" />
                        </div>
                        <!-- END product-main-image -->
                    </div>
                    <!-- END product-image -->
                    <!-- BEGIN product-info -->
                    <div class="product-info">
                        <!-- BEGIN product-info-header -->
                        <div class="product-info-header">
                            <h1 class="product-title"><span class="badge bg-primary" style="background: {{$seletedBrand->brand_color_code}}!important;">20% OFF</span>LG UN73 70 (177.8cm) 4K Smart UHD TV </h1>
                            <ul class="product-category">
                                <li><a href="#">iPhone</a></li>
                                <li>/</li>
                                <li><a href="#">mobile phone</a></li>
                                <li>/</li>
                                <li><a href="#">electronics</a></li>
                                <li>/</li>
                                <li><a href="#">lifestyle</a></li>
                            </ul>
                        </div>
                        <!-- END product-info-header -->
                        <!-- BEGIN product-warranty -->
                        <div class="product-warranty">
                            <div class="pull-right">Availability: In stock</div>
                            <div><b>1 Year</b> Local Manufacturer Warranty</div>
                        </div>
                        <!-- END product-warranty -->
                        <!-- BEGIN product-info-list -->
                        <ul class="product-info-list">
                            <li><i class="fa fa-circle"></i> Real 4K Display</li>
                            <li><i class="fa fa-circle"></i> AI ThinQ w/ Built-in Google Assistant & Alexa, Apple Airplay 2 & Homekit</li>
                            <li><i class="fa fa-circle"></i> Filmmaker Mode, HDR 10 Pro & HLG Pro</li>
                            <li><i class="fa fa-circle"></i> Gaming: Low Input Lag & HGiG Profile</li>
                            <li><i class="fa fa-circle"></i> Sports Alert & Bluetooth Surround Ready</li>
                            <li><i class="fa fa-circle"></i> WebOS w/ Unlimited OTT App Support</li>
                        </ul>
                        <!-- END product-info-list -->
                        <!-- BEGIN product-social -->
                        <div class="product-social">
                            <ul>
                                <li><a href="javascript:;" class="facebook" data-toggle="tooltip" data-trigger="hover" data-title="Facebook" data-placement="top"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="javascript:;" class="twitter" data-toggle="tooltip" data-trigger="hover" data-title="Twitter" data-placement="top"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="javascript:;" class="google-plus" data-toggle="tooltip" data-trigger="hover" data-title="Google Plus" data-placement="top"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href="javascript:;" class="whatsapp" data-toggle="tooltip" data-trigger="hover" data-title="Whatsapp" data-placement="top"><i class="fab fa-whatsapp"></i></a></li>
                                <li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="Tumblr" data-placement="top"><i class="fab fa-tumblr"></i></a></li>
                            </ul>
                        </div>
                        <!-- END product-social -->
                        <!-- BEGIN product-purchase-container -->
                        <div class="product-purchase-container">
                            <div class="product-discount">
                                <span class="discount">Rs.129990</span>
                            </div>
                            <div class="product-price">
                                <div class="price">Rs.103490</div>
                            </div>
                            <a href="checkout_cart.html" class="btn btn-inverse btn-theme btn-lg width-200" style="background: {{$seletedBrand->brand_color_code}}!important;border: none;">WHERE TO BUY</a>
                        </div>
                        <!-- END product-purchase-container -->
                    </div>
                    <!-- END product-info -->
                </div>
                <!-- END product-detail -->
                <!-- BEGIN product-tab -->
                <div class="product-tab">
                    <!-- BEGIN #product-tab -->
                    <ul id="product-tab" class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" href="#product-desc" data-toggle="tab">Product Description</a></li>
                        <li class="nav-item"><a class="nav-link" href="#product-info" data-toggle="tab">Additional Information</a></li>
                        <li class="nav-item"><a class="nav-link" href="#product-reviews" data-toggle="tab">Rating & Reviews (5)</a></li>
                    </ul>
                    <!-- END #product-tab -->
                    <!-- BEGIN #product-tab-content -->
                    <div id="product-tab-content" class="tab-content">
                        <!-- BEGIN #product-desc -->
                        <div class="tab-pane fade active show" id="product-desc">
                            <!-- BEGIN product-desc -->
                            <div class="product-desc">
                                <div class="image">
                                    <img src="{{asset('assets/img/product/product-desc_img1.png')}}" alt="" />
                                </div>
                                <div class="desc">
                                    <h4>HDR 10 Pro & HLG Pro</h4>
                                    <p>
                                        Enjoy content in lifelike high definition
                                        LG UHD TVs provide optimal HDR picture quality with the support of major HDR formats including HDR 10 Pro and HLG Pro.
                                    </p>
                                </div>
                            </div>
                            <!-- END product-desc -->
                            <!-- BEGIN product-desc -->
                            <div class="product-desc right">
                                <div class="image">
                                    <img src="{{asset('assets/img/product/product-desc_img2.png')}}" alt="" />
                                </div>
                                <div class="desc">
                                    <h4>Ultra Surround</h4>
                                    <p>
                                        Dive into the art of sound
                                        Multiple virtual audio channels create a more immersive sound experience. Enjoy subtle dialogue and sophisticated sound as if you were in the scene itself.
                                    </p>
                                </div>
                            </div>
                            <!-- END product-desc -->
                            <!-- BEGIN product-desc -->
                            <div class="product-desc">
                                <div class="image">
                                    <img src="{{asset('assets/img/product/product-desc_img3.png')}}" alt="" />
                                </div>
                                <div class="desc">
                                    <h4>Unlimited Entertainment</h4>
                                    <p>
                                        Endless content. Endless enjoyment
                                        Dive into the wide variety of content to choose from Disney+ Hotstar, the Apple TV app, Netflix and Prime Videos. From the latest movies, TV shows, and documentaries, live sports and more, find them all here in one place. Just sit back and enjoy.
                                    </p>
                                </div>
                            </div>
                            <!-- END product-desc -->
                            <!-- BEGIN product-desc -->
                            <div class="product-desc right">
                                <div class="image">
                                    <img src="{{asset('assets/img/product/product-desc_img4.png')}}" alt="" />
                                </div>
                                <div class="desc">
                                    <h4>HGiG profile for HDR Gaming</h4>
                                    <p>
                                        Immerse yourself in the game
                                        HGiG recognizes tv performance and picture quality then tunes HDR graphics to provide the ultimate HDR gaming experience.
                                    </p>
                                </div>
                            </div>
                            <!-- END product-desc -->
                            <!-- BEGIN product-desc -->
                            <div class="product-desc">
                                <div class="image">
                                    <img src="{{asset('assets/img/product/product-desc_img5.png')}}" alt="" />
                                </div>
                                <div class="desc">
                                    <h4>Low Input Lag</h4>
                                    <p>
                                        Take complete control without delays
                                        LG UHD TVs allow users to take full and instant control of games without any delays.
                                    </p>
                                </div>
                            </div>
                            <!-- END product-desc -->
                        </div>
                        <!-- END #product-desc -->
                        <!-- BEGIN #product-info -->
                        <div class="tab-pane fade" id="product-info">
                            <!-- BEGIN table-responsive -->
                            <div class="table-responsive">
                                <!-- BEGIN table-product -->
                                <table class="table table-product table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>iPhone 6s</th>
                                        <th>iPhone 6s Plus</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="field">Capacity</td>
                                        <td>
                                            16GB<br />
                                            64GB<br />
                                            128GB
                                        </td>
                                        <td>
                                            16GB<br />
                                            64GB<br />
                                            128GB
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Weight and Dimensions</td>
                                        <td>
                                            5.44 inches (138.3 mm) x 2.64 inches (67.1 mm) x 0.28 inch (7.1 mm)<br />
                                            Weight: 5.04 ounces (143 grams)
                                        </td>
                                        <td>
                                            6.23 inches (158.2 mm) x 3.07 inches (77.9 mm) x 0.29 inch (7.3 mm)<br />
                                            Weight: 6.77 ounces (192 grams)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Display</td>
                                        <td>
                                            Retina HD display with 3D Touch<br />
                                            4.7-inch (diagonal) LED-backlit widescreen<br />
                                            1334-by-750-pixel resolution at 326 ppi<br />
                                            1400:1 contrast ratio (typical)<br />
                                            <br />
                                            <b>Both models:</b><br />
                                            500 cd/m2 max brightness (typical)<br />
                                            Full sRGB standard<br />
                                            Dual-domain pixels for wide viewing angles<br />
                                            Fingerprint-resistant oleophobic coating on front<br />
                                            Support for display of multiple languages and characters simultaneously<br />
                                            Display Zoom<br />
                                            Reachability
                                        </td>
                                        <td>
                                            Retina HD display with 3D Touch<br />
                                            5.5-inch (diagonal) LED-backlit widescreen<br />
                                            1920-by-1080-pixel resolution at 401 ppi<br />
                                            1300:1 contrast ratio (typical)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Chip</td>
                                        <td colspan="2">
                                            A9 chip with 64-bit architecture Embedded M9 motion coprocessor
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">iSight Camera</td>
                                        <td colspan="2">
                                            New 12-megapixel iSight camera with 1.22� pixels<br />
                                            Live Photos<br />
                                            Autofocus with Focus Pixels<br />
                                            Optical image stabilization (iPhone 6s Plus only)<br />
                                            True Tone flash<br />
                                            Panorama (up to 63 megapixels)<br />
                                            Auto HDR for photos<br />
                                            Exposure control<br />
                                            Burst mode<br />
                                            Timer mode<br />
                                            �/2.2 aperture<br />
                                            Five-element lens<br />
                                            Hybrid IR filter<br />
                                            Backside illumination sensor<br />
                                            Sapphire crystal lens cover<br />
                                            Auto image stabilization<br />
                                            Improved local tone mapping<br />
                                            Improved noise reduction<br />
                                            Face detection<br />
                                            Photo geotagging
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Video Recording</td>
                                        <td colspan="2">
                                            4K video recording (3840 by 2160) at 30 fps<br />
                                            1080p HD video recording at 30 fps or 60 fps<br />
                                            720p HD video recording at 30 fps<br />
                                            Optical image stabilization for video (iPhone 6s Plus only)<br />
                                            True Tone flash<br />
                                            Slo-mo video support for 1080p at 120 fps and 720p at 240 fps<br />
                                            Time-lapse video with stabilization<br />
                                            Cinematic video stabilization (1080p and 720p)<br />
                                            Continuous autofocus video<br />
                                            Improved noise reduction<br />
                                            Take 8MP still photos while recording 4K video<br />
                                            Playback zoom<br />
                                            3x zoom<br />
                                            Face detection<br />
                                            Video geotagging
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- END table-product -->
                            </div>
                            <!-- END table-responsive -->
                        </div>
                        <!-- END #product-info -->
                        <!-- BEGIN #product-reviews -->
                        <div class="tab-pane fade" id="product-reviews">
                            <!-- BEGIN row -->
                            <div class="row row-space-30">
                                <!-- BEGIN col-7 -->
                                <div class="col-md-7 mb-4 mb-lg-0">
                                    <!-- BEGIN review -->
                                    <div class="review">
                                        <div class="review-info">
                                            <div class="review-icon"><img src="../assets/img/user/user-1.jpg" alt="" /></div>
                                            <div class="review-rate">
                                                <ul class="review-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                </ul>
                                                (4/5)
                                            </div>
                                            <div class="review-name">Terry</div>
                                            <div class="review-date">24/05/2016 7:40am</div>
                                        </div>
                                        <div class="review-title">
                                            What does �SIM-free� mean?
                                        </div>
                                        <div class="review-message">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in imperdiet augue. Integer non aliquam eros. Cras vehicula nec sapien pretium sagittis. Pellentesque feugiat lectus non malesuada aliquam. Etiam id tortor pretium, dictum leo at, malesuada tortor.
                                        </div>
                                    </div>
                                    <!-- END review -->
                                    <!-- BEGIN review -->
                                    <div class="review">
                                        <div class="review-info">
                                            <div class="review-icon"><img src="../assets/img/user/user-2.jpg" alt="" /></div>
                                            <div class="review-rate">
                                                <ul class="review-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                </ul>
                                                (3/5)
                                            </div>
                                            <div class="review-name">George</div>
                                            <div class="review-date">24/05/2016 8:40am</div>
                                        </div>
                                        <div class="review-title">
                                            When I buy iPhone from apple.com, is it tied to a carrier or does it come �unlocked�?
                                        </div>
                                        <div class="review-message">
                                            In mauris leo, maximus at pellentesque vel, pharetra vel risus. Aenean in semper velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi volutpat mattis neque, at molestie tellus ultricies quis. Ut lobortis odio nec nunc ullamcorper, vitae faucibus augue semper. Sed luctus lobortis nulla ac volutpat. Mauris blandit scelerisque sem.
                                        </div>
                                    </div>
                                    <!-- END review -->
                                    <!-- BEGIN review -->
                                    <div class="review">
                                        <div class="review-info">
                                            <div class="review-icon"><img src="../assets/img/user/user-3.jpg" alt="" /></div>
                                            <div class="review-rate">
                                                <ul class="review-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                </ul>
                                                (5/5)
                                            </div>
                                            <div class="review-name">Steve</div>
                                            <div class="review-date">23/05/2016 8:40am</div>
                                        </div>
                                        <div class="review-title">
                                            Where is the iPhone Upgrade Program available?
                                        </div>
                                        <div class="review-message">
                                            Duis ut nunc sem. Integer efficitur, justo sit amet feugiat hendrerit, arcu nisl elementum dui, in ultricies erat quam at mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec nec ultrices tellus. Mauris elementum venenatis volutpat.
                                        </div>
                                    </div>
                                    <!-- END review -->
                                    <!-- BEGIN review -->
                                    <div class="review">
                                        <div class="review-info">
                                            <div class="review-icon"><img src="../assets/img/user/user-4.jpg" alt="" /></div>
                                            <div class="review-rate">
                                                <ul class="review-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                    <li class=""><i class="far fa-star"></i></li>
                                                </ul>
                                                (2/5)
                                            </div>
                                            <div class="review-name">Alfred</div>
                                            <div class="review-date">23/05/2016 10.02am</div>
                                        </div>
                                        <div class="review-title">
                                            Can I keep my current service plan if I choose the iPhone Upgrade Program?
                                        </div>
                                        <div class="review-message">
                                            Donec vel fermentum quam. Vivamus scelerisque enim eget tristique auctor. Vivamus tempus, turpis iaculis tempus egestas, leo augue hendrerit tellus, et efficitur neque massa at neque. Aenean efficitur eleifend orci at ornare.
                                        </div>
                                    </div>
                                    <!-- END review -->
                                    <!-- BEGIN review -->
                                    <div class="review">
                                        <div class="review-info">
                                            <div class="review-icon"><img src="../assets/img/user/user-5.jpg" alt="" /></div>
                                            <div class="review-rate">
                                                <ul class="review-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                </ul>
                                                (5/5)
                                            </div>
                                            <div class="review-name">Edward</div>
                                            <div class="review-date">22/05/2016 9.30pm</div>
                                        </div>
                                        <div class="review-title">
                                            I have an existing carrier contract or installment plan. Can I purchase with the iPhone Upgrade Program
                                        </div>
                                        <div class="review-message">
                                            Aliquam consequat ut turpis non interdum. Integer blandit erat nec sapien sollicitudin, a fermentum dui venenatis. Nullam consequat at enim et aliquet. Cras mattis turpis quis eros volutpat tristique vel a ligula. Proin aliquet leo mi, et euismod metus placerat sit amet.
                                        </div>
                                    </div>
                                    <!-- END review -->
                                </div>
                                <!-- END col-7 -->
                                <!-- BEGIN col-5 -->
                                <div class="col-md-5">
                                    <!-- BEGIN review-form -->
                                    <div class="review-form">
                                        <form action="product_detail.html" name="review_form" method="POST">
                                            <h2>Write a review</h2>
                                            <div class="form-group">
                                                <label for="name">Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="name" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Title <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="email" />
                                            </div>
                                            <div class="form-group">
                                                <label for="review">Review <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="8" id="review"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Rating  <span class="text-danger">*</span></label>
                                                <div class="rating rating-selection" data-rating="true" data-target="rating">
                                                    <i class="far fa-star" data-value="2"></i>
                                                    <i class="far fa-star" data-value="4"></i>
                                                    <i class="far fa-star" data-value="6"></i>
                                                    <i class="far fa-star" data-value="8"></i>
                                                    <i class="far fa-star" data-value="10"></i>
														<span class="rating-comment">
															<span class="rating-comment-tooltip">Click to rate</span>
														</span>
                                                </div>
                                                <select name="rating" class="hide">
                                                    <option value="2">2</option>
                                                    <option value="4">4</option>
                                                    <option value="6">6</option>
                                                    <option value="8">8</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-inverse btn-theme btn-lg">Submit Review</button>
                                        </form>
                                    </div>
                                    <!-- END review-form -->
                                </div>
                                <!-- END col-5 -->
                            </div>
                            <!-- END row -->
                        </div>
                        <!-- END #product-reviews -->
                    </div>
                    <!-- END #product-tab-content -->
                </div>
                <!-- END product-tab -->
            </div>
            <!-- END product -->
            <!-- BEGIN similar-product -->
            <h4 class="m-b-15 m-t-30">You Might Also Like</h4>
            <div class="row row-space-10">
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr1.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">27% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG UM72 65 (165.1cm) 4K Smart UHD TV</a>
                            </h4>
                            <p class="item-desc">65UM7290PTD</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.101990</div>
                            <div class="item-discount-price">Rs.139990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr2.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">15% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product.html">LG UN73 70 (177.8cm) 4K Smart UHD TV</a>
                            </h4>
                            <p class="item-desc">70UN7300PTC</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.169990</div>
                            <div class="item-discount-price">Rs.189990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr3.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">10% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product.html">LG Nano95 75 (190.5cm) 8K NanoCell TV</a>
                            </h4>
                            <p class="item-desc">75NANO95TNA</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.189990</div>
                            <div class="item-discount-price">Rs.199990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr4.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">15% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product_detail.html">LG Nano99 75 (190.5cm) 8K NanoCell TV</a>
                            </h4>
                            <p class="item-desc">75NANO99TNA</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.1099990</div>
                            <div class="item-discount-price">Rs.1129990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr5.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">12% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product.html">LG UN71 55 (139.7cm) 4K Smart UHD TV</a>
                            </h4>
                            <p class="item-desc">55UN7190PTA</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.79990</div>
                            <div class="item-discount-price">Rs.84990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
                <div class="col-lg-2 col-md-4">
                    <!-- BEGIN item -->
                    <div class="item item-thumbnail">
                        <a href="product_detail.html" class="item-image">
                            <img src="{{asset('assets/img/product/pr6.png')}}" alt="">
                            <div class="discount"  style="background: {{$seletedBrand->brand_color_code}}!important;">20% OFF</div>
                        </a>
                        <div class="item-info">
                            <h4 class="item-title">
                                <a href="product.html">LG SM86 65 (165.1cm) 4K NanoCell TV</a>
                            </h4>
                            <p class="item-desc">65SM8600PTA</p>
                            <div class="item-price"  style="color: {{$seletedBrand->brand_color_code}}!important;">Rs.206990</div>
                            <div class="item-discount-price">Rs.256990</div>
                        </div>
                    </div>
                    <!-- END item -->
                </div>
            </div>
            <!-- END similar-product -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #product -->



@endsection
