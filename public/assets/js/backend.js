$(document).on("submit", "form.delete-data-ajax", function (e) {
    e.preventDefault();
    var current_form = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((value) => {
        var request_data = {};

        request_data["_token"] = current_form.find("input[name=_token]").val();

        $.ajax({
            type: current_form.attr("method"),
            url: current_form.attr("action"),
            data: request_data,
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    location.reload();
                }
                if (data.status === "fail") {
                    swal("Cannot Delete!", data.message, "error");
                }
            },
        });

        swal("Deleted!", "Deleted Successfully!", "success");
    });

    return false;
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blah").attr("src", e.target.result);
            $("#blah").removeClass("d-none");
        };

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

$("#imgInp").change(function () {
    readURL(this);
});
CKEDITOR.replace("editor1", {
    filebrowserBrowseUrl: "/browser/browse.php",
    filebrowserUploadUrl: "/uploader/upload.php",
});

