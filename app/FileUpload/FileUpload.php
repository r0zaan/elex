<?php



namespace App\FileUpload;


class FileUpload
{

    public static function uploadFile($file, $filePath)
    {
        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }
        $path = '' . $filePath . '/' . \Str::random(6) . '_' . time() . '.' . $file->getClientOriginalExtension();
        file_put_contents($path, file_get_contents($file));
        return $path;
    }
    public static function deleteImage($paths)
    {
        foreach ($paths as $path) {
            if (\File::exists(public_path($path))) {
                \File::delete(public_path($path));
            }
        }
    }
}
