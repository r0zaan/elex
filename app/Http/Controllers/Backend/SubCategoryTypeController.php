<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\SubCategory;
use App\Models\SubCategoryType;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;

class SubCategoryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $subCategoryTypes = SubCategoryType::where('brand_id', $brand->id)->latest()->get();
        return view('backend.subCategoryTypes.index',compact('subCategoryTypes', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        $subCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
            $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
            return $subCategory;
        })->pluck('name', 'id');

        return view('backend.subCategoryTypes.create', compact('subCategories', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'sub_category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $inputs['slug'] = SlugService::createSlug(SubCategoryType::class, 'slug', $request->name);
        SubCategoryType::create($inputs);
        return redirect()->route('sub-category-types.index', ['brand_id' => $brand->id])->with('success','Sub Category Type created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategoryType $subCategoryType)
    {
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.subCategoryTypes.show',compact('subCategoryType', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategoryType $subCategoryType)
    {
        $brand = Brand::find($_GET['brand_id']);
        $subCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
            $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
            return $subCategory;
        })->pluck('name', 'id');

        return view('backend.subCategoryTypes.edit', compact('subCategoryType', 'subCategories', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategoryType $subCategoryType)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'sub_category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $subCategoryType->update($inputs);
        return redirect()->route('sub-category-types.index', ['brand_id' => $brand->id])->with('success','Sub Category Type Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategoryType $subCategoryType)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        $subCategoryType->delete();
        session()->flash('success', 'Sub Category Type Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('sub-category-types.index', ['brand_id' => $brand->id])
        ];
    }
}
