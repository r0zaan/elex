<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\ProductFeature;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ProductFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $features = ProductFeature::where('brand_id', $brand->id)->latest()->get();
        return view('backend.productFeature.index', compact('features', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        $subCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
            $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
            return $subCategory;
        })->pluck('name', 'id');
        return view('backend.productFeature.create', compact('subCategories', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'sub_category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $inputs['show_as_filter'] = $request->has('show_as_filter');
        $inputs['brand_id'] = $brand->id;
        ProductFeature::create($inputs);
        return redirect()->route('product-features.index', ['brand_id' => $brand->id])->with('success','Product Feature Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($_GET['brand_id']);
        $feature = ProductFeature::find($id);
        return view('backend.productFeature.show',compact('feature', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = ProductFeature::find($id);
        $brand = Brand::find($_GET['brand_id']);
        $subCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
            $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
            return $subCategory;
        })->pluck('name', 'id');

        return view('backend.productFeature.edit',compact('feature', 'brand', 'subCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'sub_category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $feature = ProductFeature::find($id);
        $inputs = $request->all();
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $inputs['show_as_filter'] = $request->has('show_as_filter');
        $inputs['brand_id'] = $brand->id;
        $feature->update($inputs);
        return redirect()->route('product-features.index', ['brand_id' => $brand->id])->with('success','Product Feature Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        $feature = ProductFeature::find($id);
        $feature->delete();
        session()->flash('success', 'Product Feature Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('product-features.index', ['brand_id' => $brand->id])
        ];
    }


}
