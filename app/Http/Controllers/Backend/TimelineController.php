<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\Timeline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TimelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timelines = Timeline::latest()->get();
        return view('backend.timeline.index',compact('timelines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.timeline.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'year' => 'required',
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096',
        ]);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');

        if ($request->hasFile('image')) {
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/timeline');
        }
        Timeline::create($inputs);
        return redirect()->route('timelines.index')->with('success','Timeline created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Timeline  $timeline
     * @return \Illuminate\Http\Response
     */
    public function show(Timeline $timeline)
    {
        return view('backend.timeline.show',compact('timeline'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Timeline  $timeline
     * @return \Illuminate\Http\Response
     */
    public function edit(Timeline $timeline)
    {
        return view('backend.timeline.edit',compact('timeline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Timeline  $timeline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timeline $timeline)
    {
        $this->validate($request,[
            'year' => 'required',
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096',
        ]);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');

        if ($request->hasFile('image')) {
            File::delete($timeline->image);
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/timeline');
        }else{
            $inputs['image'] = $timeline->image;
        }
        $timeline->update($inputs);
        return redirect()->route('timelines.index')->with('success','Timeline Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timeline  $timeline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timeline $timeline)
    {
        if (!request()->ajax()) {
            return false;
        }
        $timeline->delete();
        session()->flash('success', 'Timeline Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('timelines.index')
        ];
    }
}
