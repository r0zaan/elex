<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $subCategories = SubCategory::where('brand_id', $brand->id)->latest()->get();
        return view('backend.subCategories.index',compact('subCategories', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        $categories = Category::orderBy('name', 'asc')->where('brand_id', $brand->id)->pluck('name', 'id');
        return view('backend.subCategories.create', compact('categories', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $inputs['slug'] = SlugService::createSlug(SubCategory::class, 'slug', $request->name);
        SubCategory::create($inputs);
        return redirect()->route('sub-categories.index', ['brand_id' => $brand->id])->with('success','Sub Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.subCategories.show',compact('subCategory', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        $brand = Brand::find($_GET['brand_id']);
        $categories = Category::orderBy('name', 'asc')->where('brand_id', $brand->id)->pluck('name', 'id');

        return view('backend.subCategories.edit', compact('subCategory', 'categories', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'category_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $subCategory->update($inputs);
        return redirect()->route('sub-categories.index', ['brand_id' => $brand->id])->with('success','Sub Category Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        $subCategory->delete();
        session()->flash('success', 'Sub Category Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('sub-categories.index', ['brand_id' => $brand->id])
        ];
    }
}
