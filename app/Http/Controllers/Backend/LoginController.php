<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('backend.login.index');
    }

    public function login(Request $request){

        $this->validate($request,[
            'email' =>'required',
            'password' =>'required',
        ]);

        $email = $request['email'];
        $password = $request['password'];
        if (
            Auth::attempt(['email' => $email, 'password' => $password])) {
                session()->flash('success', 'Login Successfully');
                return redirect()->route('admin.dashboard');
        }
        session()->flash('error', 'Username and Password does not match');
        return redirect()->back();
    }


    public function logout()
    {
        Auth::logout();
        Session::flush();
        session()->flash('success', 'Logout Successfully');
        return redirect()->route('admin.login');
    }
}
