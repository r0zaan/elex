<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\News;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::latest()->get();
        return view('backend.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096|required',
        ]);
        $inputs = $request->all();

        if ($request->hasFile('image')) {
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/news');
        }
        $insert = [
            'title' => $inputs['title'],
            'date' => $inputs['date'],
            'body' => $inputs['body'],
            'image' => $inputs['image'],
            'meta_title' => $inputs['meta_title'],
            'meta_description'  => $inputs['meta_description'],
            'meta_keywords'  => $inputs['meta_keywords'],
            'status' => $request->has('status'),
            'slug' => SlugService::createSlug(News::class, 'slug', $request->title)
        ];

        News::insertGetId($insert);
        return redirect()->route('news.index')->with('success','News created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('backend.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('backend.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096',
        ]);
        $inputs = $request->all();
        if ($request->hasFile('image')) {
            File::delete($news->image);
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/news');
        }else{
            $inputs['image'] = $news->image;
        }
        $insert = [
            'title' => $inputs['title'],
            'date' => $inputs['date'],
            'body' => $inputs['body'],
            'image' => $inputs['image'],
            'meta_title' => $inputs['meta_title'],
            'meta_description'  => $inputs['meta_description'],
            'meta_keywords'  => $inputs['meta_keywords'],
            'status' => $request->has('status'),
            'slug' => $inputs['slug']
        ];

        $news->update($insert);
        return redirect()->route('news.index')->with('success','News Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if (!request()->ajax()) {
            return false;
        }
        File::delete($news->image);
        $news->delete();
        session()->flash('success', 'News Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('news.index')
        ];
    }
}
