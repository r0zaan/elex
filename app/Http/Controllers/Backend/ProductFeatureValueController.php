<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\ProductFeature;
use App\Models\ProductFeatureValue;
use Illuminate\Http\Request;

class ProductFeatureValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $featureValues = ProductFeatureValue::where('brand_id', $brand->id)->latest()->get();
        return view('backend.productFeatureValue.index', compact('featureValues', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        $features = ProductFeature::orderBy('name', 'asc')->get()->map(function($feature){
            $feature['name'] = $feature->name.' - '.$feature->subCategory->name.' - '.$feature->subCategory->category->name;
            return $feature;
        })->pluck('name', 'id');
        return view('backend.productFeatureValue.create', compact('features', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'value' => 'required',
            'product_feature_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');
        $inputs['brand_id'] = $brand->id;
        ProductFeatureValue::create($inputs);
        return redirect()->route('product-feature-values.index', ['brand_id' => $brand->id])->with('success','Feature Value Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($_GET['brand_id']);
        $featureValue = ProductFeatureValue::find($id);
        return view('backend.productFeatureValue.show',compact('featureValue', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featureValue = ProductFeatureValue::find($id);
        $brand = Brand::find($_GET['brand_id']);
        $features = ProductFeature::orderBy('name', 'asc')->get()->map(function($feature){
            $feature['name'] = $feature->name.' - '.$feature->subCategory->name.' - '.$feature->subCategory->category->name;
            return $feature;
        })->pluck('name', 'id');
        return view('backend.productFeatureValue.edit',compact('featureValue', 'features', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'value' => 'required',
            'product_feature_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $featureValue = ProductFeatureValue::find($id);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');
        $inputs['brand_id'] = $brand->id;
        $featureValue->update($inputs);
        return redirect()->route('product-feature-values.index', ['brand_id' => $brand->id])->with('success','Feature Value Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        $featureValue = ProductFeatureValue::find($id);
        $featureValue->delete();
        session()->flash('success', 'Feature Value Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('product-feature-values.index', ['brand_id' => $brand->id])
        ];
    }


}
