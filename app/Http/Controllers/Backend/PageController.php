<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::latest()->get();
        return view('backend.page.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $inputs = $request->all();
        $insert = [
            'title' => $inputs['title'],
            'body' => $inputs['body'],
            'meta_title' => $inputs['meta_title'],
            'meta_description'  => $inputs['meta_description'],
            'meta_keywords'  => $inputs['meta_keywords'],
            'status' => $request->has('status'),
            'slug' => SlugService::createSlug(Page::class, 'slug', $request->title)
        ];

        Page::insertGetId($insert);
        return redirect()->route('pages.index')->with('success','Page created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('backend.page.show',compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('backend.page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $inputs = $request->all();
        $insert = [
            'title' => $inputs['title'],
            'body' => $inputs['body'],
            'meta_title' => $inputs['meta_title'],
            'meta_description'  => $inputs['meta_description'],
            'meta_keywords'  => $inputs['meta_keywords'],
            'status' => $request->has('status'),
            'slug' => $inputs['slug']
        ];

        $page->update($insert);
        return redirect()->route('pages.index')->with('success','Page Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        if (!request()->ajax()) {
            return false;
        }
        $page->delete();
        session()->flash('success', 'Page Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('pages.index')
        ];
    }
}
