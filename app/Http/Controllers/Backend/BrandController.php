<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\File;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::latest()->get();
        return view('backend.brand.index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'brand_color_code' => 'required',
            'logo' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096|required',
        ]);
        $inputs = $request->all();
        $inputs['name'] = ucwords($request->name);
        $inputs['status'] = $request->has('status');
        $inputs['slug'] = SlugService::createSlug(Brand::class, 'slug', $request->name);
        if ($request->hasFile('logo')) {
            $inputs['logo'] = FileUpload::uploadFile($request->file('logo'), 'logo/brand');
        }
        Brand::create($inputs);
        return redirect()->route('brands.index')->with('success','Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('backend.brand.show',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('backend.brand.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'brand_color_code' => 'required',
            'logo' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096',
        ]);
        $inputs = $request->all();
        if ($request->hasFile('logo')) {
            File::delete($brand->logo);
            $inputs['logo'] = FileUpload::uploadFile($request->file('logo'), 'logo/brand');
        }else{
            $inputs['logo'] = $brand->logo;
        }

        $inputs['status'] = $request->has('status');
        $brand->update($inputs);
        return redirect()->route('brands.index')->with('success','Brand Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        if (!request()->ajax()) {
            return false;
        }
        File::delete($brand->logo);
        $brand->delete();
        session()->flash('success', 'Brand Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('brands.index')
        ];
    }
}
