<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\BrandPageSlider;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubCategoryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BrandPageSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);
        $sliders = BrandPageSlider::where('slider_type', $type)->where('brand_id', $brand->id)->latest()->get();
        return view('backend.brandPageSlider.index',compact('sliders', 'type', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);

        $parentCategories =null;
        switch ($type) {
            case 'Category':
                $parentCategories = Category::orderBy('name', 'asc')->where('brand_id', $brand->id)->pluck('name', 'id');
            break;
            case 'Sub Category':
                $parentCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
                    $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
                    return $subCategory;
                })->pluck('name', 'id');
            break;
            case 'Sub Category Type':
                $parentCategories = SubCategoryType::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategoryType){
                    $subCategoryType['name'] = $subCategoryType->name.' - '.$subCategoryType->subCategory->name.' - '.$subCategoryType->subCategory->category->name;
                    return $subCategoryType;
                })->pluck('name', 'id');
            break;
        }

        return view('backend.brandPageSlider.create', compact('type', 'brand', 'parentCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($_GET['type'] == 'Brand'){
            $this->validate($request,[
                'link_type' => 'required',
                'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096|required',
            ]);
        }else{
            $this->validate($request,[
                'link_type' => 'required',
                'slider_type_parent_id' => 'required|integer',
                'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096|required',
            ]);
        }

        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['slider_type'] = $type;
        $inputs['status'] = $request->has('status');

        switch ($type) {
            case 'Brand':
                $filePath = 'images/slider/brandPage/brand';
                break;
            case 'Category':
                $filePath = 'images/slider/brandPage/category';
                break;
            case 'Sub Category':
                $filePath = 'images/slider/brandPage/subCategory';
                break;
            case 'Sub Category Type':
                $filePath = 'images/slider/brandPage/subCategoryType';
                break;
        }

        if ($request->hasFile('image')) {
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), $filePath);
        }
        BrandPageSlider::create($inputs);
        return redirect()->route('brandPageSliders.index', ['type' => $type, 'brand_id' => $brand->id])
            ->with('success','Slider created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function show(BrandPageSlider $brandPageSlider)
    {
        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.brandPageSlider.show',compact('brandPageSlider', 'type', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function edit(BrandPageSlider $brandPageSlider)
    {
        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);

        $parentCategories =null;
        switch ($type) {
            case 'Category':
                $parentCategories = Category::orderBy('name', 'asc')->where('brand_id', $brand->id)->pluck('name', 'id');
                break;
            case 'Sub Category':
                $parentCategories = SubCategory::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategory){
                    $subCategory['name'] = $subCategory->name.' - '.$subCategory->category->name;
                    return $subCategory;
                })->pluck('name', 'id');
                break;
            case 'Sub Category Type':
                $parentCategories = SubCategoryType::orderBy('name', 'asc')->where('brand_id', $brand->id)->get()->map(function($subCategoryType){
                    $subCategoryType['name'] = $subCategoryType->name.' - '.$subCategoryType->subCategory->name.' - '.$subCategoryType->subCategory->category->name;
                    return $subCategoryType;
                })->pluck('name', 'id');
                break;
        }

        return view('backend.brandPageSlider.edit',compact('brandPageSlider', 'type', 'brand', 'parentCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BrandPageSlider $brandPageSlider)
    {
        if($_GET['type'] == 'Brand'){
            $this->validate($request,[
                'link_type' => 'required',
                'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096',
            ]);
        }else{
            $this->validate($request,[
                'link_type' => 'required',
                'slider_type_parent_id' => 'required|integer',
                'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096',
            ]);
        }



        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['slider_type'] = $type;
        $inputs['status'] = $request->has('status');

        switch ($type) {
            case 'Brand':
                $filePath = 'images/slider/brandPage/brand';
                break;
            case 'Category':
                $filePath = 'images/slider/brandPage/category';
                break;
            case 'Sub Category':
                $filePath = 'images/slider/brandPage/subCategory';
                break;
            case 'Sub Category Type':
                $filePath = 'images/slider/brandPage/subCategoryType';
                break;
        }

        if ($request->hasFile('image')) {
            File::delete($brandPageSlider->image);
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), $filePath);
        }else{
            $inputs['image'] = $brandPageSlider->image;
        }
        $brandPageSlider->update($inputs);
        return redirect()->route('brandPageSliders.index', ['type' => $type, 'brand_id' => $brand->id])->with('success','Slider Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandPageSlider $brandPageSlider)
    {
        if (!request()->ajax()) {
            return false;
        }
        $type = $_GET['type'];
        $brand = Brand::find($_GET['brand_id']);
        if($brandPageSlider->image != null){
         File::delete($brandPageSlider->image);
        }
        $brandPageSlider->delete();
        session()->flash('success', 'Slider Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('brandPageSliders.index', ['type' => $type, 'brand_id' => $brand->id])
        ];
    }
}
