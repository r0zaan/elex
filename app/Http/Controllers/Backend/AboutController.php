<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::latest()->get();
        return view('backend.about.index',compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096|required',
        ]);
        $inputs = $request->all();

        if ($request->hasFile('image')) {
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/about');
        }
        $insert = [
            'sort_number' => $inputs['sort_number'],
            'title' => $inputs['title'],
            'body' => $inputs['body'],
            'image' => $inputs['image'],
            'status' => $request->has('status'),

        ];

        About::insertGetId($insert);
        return redirect()->route('abouts.index')->with('success','About created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        return view('backend.about.show',compact('about'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        return view('backend.about.edit',compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp |max:4096',
        ]);
        $inputs = $request->all();
        if ($request->hasFile('image')) {
            File::delete($about->image);
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/about');
        }else{
            $inputs['image'] = $about->image;
        }
        $insert = [
            'sort_number' => $inputs['sort_number'],
            'title' => $inputs['title'],
            'body' => $inputs['body'],
            'image' => $inputs['image'],
            'status' => $request->has('status'),
        ];

        $about->update($insert);
        return redirect()->route('abouts.index')->with('success','About Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        if (!request()->ajax()) {
            return false;
        }
        File::delete($about->image);
        $about->delete();
        session()->flash('success', 'About Us Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('abouts.index')
        ];
    }
}
