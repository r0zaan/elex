<?php

namespace App\Http\Controllers\Backend;

use App\Models\Brand;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $categories = Category::where('brand_id', $brand->id)->latest()->get();
        return view('backend.categories.index',compact('categories', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.categories.create', compact('brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'brand_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['status'] = $request->has('status');
        $inputs['name'] = ucwords($request->name);
        $inputs['slug'] = SlugService::createSlug(Category::class, 'slug', $request->name);
        Category::create($inputs);
        return redirect()->route('categories.index', ['brand_id' => $brand->id])->with('success','Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.categories.show',compact('category', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $brand = Brand::find($_GET['brand_id']);
        return view('backend.categories.edit', compact('category', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request,[
            'name' => 'required',
            'brand_id' => 'required|integer',
        ]);
        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['brand_id'] = $brand->id;
        $inputs['status'] = $request->has('status');
        $inputs['name'] = ucwords($request->name);
        $category->update($inputs);
        return redirect()->route('categories.index', ['brand_id' => $brand->id])->with('success','Category Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        $category->delete();
        session()->flash('success', 'Category Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('categories.index', ['brand_id' => $brand->id])
        ];
    }
}
