<?php

namespace App\Http\Controllers\Backend;

use App\FileUpload\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class HomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homeSliders = HomeSlider::latest()->get();
        return view('backend.homeSlider.index',compact('homeSliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.homeSlider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096|required',
        ]);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');
        if ($request->hasFile('image')) {
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/slider');
        }
        HomeSlider::create($inputs);
        return redirect()->route('homeSliders.index')->with('success','Home Slider created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function show(HomeSlider $homeSlider)
    {
        return view('backend.homeSlider.show',compact('homeSlider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function edit(HomeSlider $homeSlider)
    {
        return view('backend.homeSlider.edit',compact('homeSlider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomeSlider $homeSlider)
    {
        $this->validate($request,[
            'type' => 'required',
            'image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096',
        ]);
        $inputs = $request->all();
        if ($request->hasFile('image')) {
            File::delete($homeSlider->image);
            $inputs['image'] = FileUpload::uploadFile($request->file('image'), 'images/slider');
        }else{
            $inputs['image'] = $homeSlider->image;
        }
        $inputs['status'] = $request->has('status');
        $homeSlider->update($inputs);
        return redirect()->route('homeSliders.index')->with('success','Home Slider Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeSlider $homeSlider)
    {
        if (!request()->ajax()) {
            return false;
        }
        if($homeSlider->image != null){
         File::delete($homeSlider->image);
        }
        $homeSlider->delete();
        session()->flash('success', 'Home Slider Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('homeSliders.index')
        ];
    }
}
