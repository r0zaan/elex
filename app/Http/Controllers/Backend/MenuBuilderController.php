<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MenuBuilder;
use Illuminate\Http\Request;

class MenuBuilderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menuBuilders = MenuBuilder::orderBy('sort_number','ASC')->whereNull('parent_id')->get();
        return view('backend.menuBuilder.index',compact('menuBuilders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parents = MenuBuilder::whereNull('brand_id')->whereIn('type',['Large Dropdown','Dropdown'])->get()->map(function($parent){
            $parent['name'] =   $parent->name .' - '.$parent->type;
            return $parent;
        })->pluck('name','id');
        return view('backend.menuBuilder.create',compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
        ]);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');
        MenuBuilder::create($inputs);
        return redirect()->route('menuBuilders.index')->with('success','Menu created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MenuBuilder  $menuBuilder
     * @return \Illuminate\Http\Response
     */
    public function show(MenuBuilder $menuBuilder)
    {
        return view('backend.menuBuilder.show',compact('menuBuilder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MenuBuilder  $menuBuilder
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuBuilder $menuBuilder)
    {
        $parents = MenuBuilder::whereNull('brand_id')->whereIn('type',['Large Dropdown','Dropdown'])->get()->map(function($parent){
            $parent['name'] =   $parent->name .' - '.$parent->type;
            return $parent;
        })->pluck('name','id');
        $types = [
            'Large Dropdown' => 'Large Dropdown',
            'Brand' => 'Brand',
            'Dropdown' => 'Dropdown',
            'Slug' => 'Slug',
            'Link' => 'Link',
        ];
        if($menuBuilder->parent_id != null || $menuBuilder->parent_id != ""){
            if($menuBuilder->parentMenu->type == "Large Dropdown"){
                $types = [
                    'Dropdown' => 'Dropdown',
                    'Slug' => 'Slug',
                    'Link' => 'Link',
                ];
            }elseif($menuBuilder->parentMenu->type == "Dropdown"){
                $types = [
                    'Dropdown' => 'Dropdown',
                    'Slug' => 'Slug',
                    'Link' => 'Link',
                ];
            }
        }

        return view('backend.menuBuilder.edit',compact('menuBuilder','parents','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MenuBuilder  $menuBuilder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuBuilder $menuBuilder)
    {
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
        ]);
        $inputs = $request->all();
        $inputs['status'] = $request->has('status');
        $menuBuilder->update($inputs);
        return redirect()->route('menuBuilders.index')->with('success','Menu Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MenuBuilder  $menuBuilder
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuBuilder $menuBuilder)
    {
        if (!request()->ajax()) {
            return false;
        }
        $menuBuilder->delete();
        session()->flash('success', 'Menu Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('menuBuilders.index')
        ];
    }

    public function copyMenu(MenuBuilder $menuBuilder)
    {
        $parents = MenuBuilder::whereNull('brand_id')->whereIn('type',['Large Dropdown','Dropdown'])->get()->map(function($parent){
            $parent['name'] =   $parent->name .' - '.$parent->type;
            return $parent;
        })->pluck('name','id');
        $types = [
            'Large Dropdown' => 'Large Dropdown',
            'Brand' => 'Brand',
            'Dropdown' => 'Dropdown',
            'Slug' => 'Slug',
            'Link' => 'Link',
        ];
        if($menuBuilder->parent_id != null || $menuBuilder->parent_id != ""){
            if($menuBuilder->parentMenu->type == "Large Dropdown"){
                $types = [
                    'Dropdown' => 'Dropdown',
                    'Slug' => 'Slug',
                    'Link' => 'Link',
                ];
            }elseif($menuBuilder->parentMenu->type == "Dropdown"){
                $types = [
                    'Dropdown' => 'Dropdown',
                    'Slug' => 'Slug',
                    'Link' => 'Link',
                ];
            }
        }

        return view('backend.menuBuilder.copy',compact('menuBuilder','parents','types'));
    }
    public function checkparent($parentId)
    {
        $parent = MenuBuilder::find($parentId);
        return $parent['type'];
    }
}
