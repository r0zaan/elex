<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductFeature;
use Illuminate\Http\Request;
use App\FileUpload\FileUpload;
use App\Models\ProductImage;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    private $destinationPath = 'images/products';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::find($_GET['brand_id']);
        $products = Product::where('brand_id', $brand->id)->latest()->get();
        return view('backend.product.index', compact('brand', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::find($_GET['brand_id']);
        $categories = Category::where('brand_id', $brand->id)->orderBy('name', 'asc')->pluck('name', 'id');
        $productFeatures = ProductFeature::where('brand_id', $brand->id)->orderBy('name', 'asc')->pluck('name', 'id');
        return view('backend.product.create', compact('brand', 'categories', 'productFeatures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required',
            'category_id' => 'required|integer',
            'featured' => 'required|integer',
            'model_number' => 'required',
            'price' => 'required',
            'featured_image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096|required',
        ]);

        $featureValueRequests = $request->only('product_feature', 'feature_value', 'sort_number');
        $feature = $featureValueRequests['product_feature'];
        $featureValue = $featureValueRequests['feature_value'];
        $sortNumber = $featureValueRequests['sort_number'];

        foreach ($feature as $key => $value) {
            if (($feature[$key] != '' && $featureValue[$key] == '') || ($feature[$key] == '' && $featureValue[$key] != '')) {
                $this->validate(
                    $request,
                    [
                        'product_feature[]' => 'required',
                    ],
                    [
                        'product_feature[].required' => 'Insert All Fields .'
                    ]
                );
            }
        }

        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['product_name'] = ucwords($request->product_name);
        $inputs['brand_id'] = $brand->id;
        $inputs['availability'] = $request->has('availability');
        $inputs['status'] = $request->has('status');
        $inputs['featured_image'] = FileUpload::uploadFile($request->file('featured_image'), 'images/product/' . $brand->name);
        $product = Product::create($inputs);
        foreach ($feature as $key => $value) {
            $product->productSpecifications()->create([
                'product_feature_id' => $value,
                'product_feature_value_id' => null,
                'value' => $featureValue[$key],
                'sort_number' => $sortNumber[$key],
            ]);
        }
        if ($request->hasFile('image_path')) {
            $files = $request->file('image_path');
            foreach ($files as $file) {
                $multiimage  = FileUpload::uploadFile($file, 'images/product/' . $brand->name);
                $product->productImages()->create([
                    'image_path' =>  $multiimage
                ]);
            }
        }
        return redirect()->route('products.index', ['brand_id' => $brand->id])->with('success', 'Product Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $brand = Brand::find($_GET['brand_id']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $brand = Brand::find($_GET['brand_id']);
        $categories = Category::where('brand_id', $brand->id)->orderBy('name', 'asc')->pluck('name', 'id');
        $subCategories = $product->category->subCategories()->orderBy('name', 'asc')->pluck('name', 'id');
        $subCategoryTypes = ($product->subCategory != null && $product->subCategory != "") ? $product->subCategory->subCategoryTypes()->orderBy('name', 'asc')->pluck('name', 'id') : [];
        $productFeatures = ProductFeature::where('brand_id', $brand->id)->orderBy('name', 'asc')->pluck('name', 'id');
        return view('backend.product.edit', compact('brand', 'categories', 'productFeatures', 'product', 'subCategories', 'subCategoryTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required',
            'category_id' => 'required|integer',
            'featured' => 'required|integer',
            'model_number' => 'required',
            'price' => 'required',
            'featured_image' => 'mimes:jpeg,png,bmp,tiff,jpg,webp|max:4096',
        ]);

        $featureValueRequests = $request->only('product_feature', 'feature_value', 'sort_number');
        $feature = $featureValueRequests['product_feature'];
        $featureValue = $featureValueRequests['feature_value'];
        $sortNumber = $featureValueRequests['sort_number'];

        foreach ($feature as $key => $value) {
            if (($feature[$key] != '' && $featureValue[$key] == '') || ($feature[$key] == '' && $featureValue[$key] != '')) {
                $this->validate(
                    $request,
                    [
                        'product_feature[]' => 'required',
                    ],
                    [
                        'product_feature[].required' => 'Insert All Fields .'
                    ]
                );
            }
        }

        $brand = Brand::find($_GET['brand_id']);
        $inputs = $request->all();
        $inputs['product_name'] = ucwords($request->product_name);
        $inputs['brand_id'] = $brand->id;
        $inputs['availability'] = $request->has('availability');
        $inputs['status'] = $request->has('status');
        if ($request->hasFile('featured_image')) {
            File::delete($product->featured_image);
            $inputs['featured_image'] = FileUpload::uploadFile($request->file('featured_image'), 'images/product/' . $brand->name);
        } else {
            $inputs['featured_image'] = $product->featured_image;
        }
        $product->update($inputs);
        $product->productSpecifications()->delete();
        foreach ($feature as $key => $value) {
            $product->productSpecifications()->create([
                'product_feature_id' => $value,
                'product_feature_value_id' => null,
                'value' => $featureValue[$key],
                'sort_number' => $sortNumber[$key],
            ]);
        }
        if ($request->hasFile('image_path')) {
            $files = $request->file('image_path');
            foreach ($files as $file) {
                $multiimage  = FileUpload::uploadFile($file, 'images/product/' . $brand->name);
                $product->productImages()->create([
                    'image_path' =>  $multiimage
                ]);
            }
        }

        return redirect()->route('products.index', ['brand_id' => $brand->id])->with('success', 'Product Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (!request()->ajax()) {
            return false;
        }
        $brand = Brand::find($_GET['brand_id']);
        File::delete($product->featured_image);
        $product->delete();
        session()->flash('success', 'Product Deleted.');
        return [
            'status' => 'success',
            'return_url' => route('products.index', ['brand_id' => $brand->id])
        ];
    }
    public function destroyProductImage($imageId)
    {
        if (!request()->ajax()) {
            return false;
        }

        $image = ProductImage::find($imageId);
        File::delete($image->image_path);
        $image->delete();
        return [
            'status' => 'success',
        ];
    }
}
