<?php

namespace App\Http\Controllers\Frontend\Brand;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index($slugString, $productModel){
        $seletedBrand  = Brand::findBySlugOrFail($slugString);

        return view('frontend.brand.productDetail', compact('seletedBrand'))->with('title', 'Product Detail');

    }
}
