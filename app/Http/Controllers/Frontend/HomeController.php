<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Brand;
use App\Models\HomeSlider;
use App\Models\News;
use App\Models\Timeline;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $sliders = HomeSlider::orderBy('sort_number','ASC')->Active()->get();
        $abouts = About::orderBy('sort_number','ASC')->Active()->get();
        $news = News::orderBy('date','DESC')->Active()->get();
        $brands = Brand::orderBy('sort_number','ASC')->Active()->get();
        $timelines = Timeline::orderBy('year','DESC')->Active()->get();
        return view('frontend.home.index',compact('brands','timelines','sliders','news','abouts'));
    }
}
