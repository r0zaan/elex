<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index($slugString){
        $seletedBrand  = Brand::findBySlugOrFail($slugString);
        if($seletedBrand->status == 1){
            return view('frontend.brand.index',compact('seletedBrand'))->with('title', 'CG|Electronics '.ucwords($seletedBrand->name));
        }else{
            abort(404);
        }
    }
}
