<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class CustomPageController extends Controller
{
    public function index($slugString){
        $page  = Page::findBySlugOrFail($slugString);
        if($page->status == 1){
            return view('frontend.customPage.index',compact('page'));
        }else{
            abort(404);
        }
    }
}
