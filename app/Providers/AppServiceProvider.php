<?php

namespace App\Providers;

use App\Models\Brand;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.layouts.master', function($view){
            $view->with('brands', Brand::select('logo', 'slug')->where('status', '1')->get());
        });

    }
}
