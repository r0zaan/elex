<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status', 'show_as_filter', 'sort_number', 'brand_id', 'sub_category_id'
    ];


    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }

    public function productFeatureValues(){
        return $this->hasMany(ProductFeatureValue::class);
    }

    public function scopeActive($query){
        return $query->where('status', 1);
    }


}
