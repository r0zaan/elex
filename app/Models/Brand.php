<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Brand extends Model
{
    use HasFactory;
    use Sluggable;
    use SluggableScopeHelpers;
    protected $fillable = [
        'name',
        'code',
        'logo',
        'slogan',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status',
        'slug',
        'brand_color_code'
    ];


    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
