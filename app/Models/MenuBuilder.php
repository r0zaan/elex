<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuBuilder extends Model
{
    use HasFactory;


    protected $fillable = [
        'parent_id',
        'sort_number',
        'name',
        'brand_id',
        'type',
        'go_to',
        'same_page',
        'status',
    ];


    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public function childMenus(){
        return $this->hasMany(MenuBuilder::class,'parent_id');
    }
    public function parentMenu(){
        return $this->belongsTo(MenuBuilder::class,'parent_id');
    }
}
