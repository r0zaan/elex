<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_id',
        'category_id',
        'sub_category_id',
        'sub_category_type_id',
        'product_name',
        'model_number',
        'product_code',
        'search_query_string',
        'short_description',
        'long_description',
        'price',
        'discounted_price',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'featured',
        'featured_image',
        'new_period',
        'availability',
        'status',
    ];


    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }

    public function subCategoryType(){
        return $this->belongsTo(SubCategoryType::class);
    }

    public function scopeActive($query){
        return $query->where('status', 1);
    }


    public function productImages(){
        return $this->hasMany(ProductImage::class);
    }


    public function productSpecifications(){
        return $this->hasMany(ProductSpecification::class);
    }



}
