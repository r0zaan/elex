<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFeatureValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_feature_id',
        'value',
        'status',
        'sort_number',
        'brand_id',
    ];


    public function productFeature(){
        return $this->belongsTo(ProductFeature::class);
    }

}
