<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class SubCategory extends Model
{
    use HasFactory;
    use Sluggable;
    use SluggableScopeHelpers;
    protected $fillable = [
        'name',
        'brand_id',
        'category_id',
        'sort_order',
        'code',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status',
        'slug',
    ];


    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


    public function subCategoryTypes(){
        return $this->hasMany(SubCategoryType::class);
    }


}
