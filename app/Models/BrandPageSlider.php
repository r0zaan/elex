<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class BrandPageSlider extends Model
{
    use HasFactory;
    use Sluggable;
    use SluggableScopeHelpers;
    protected $fillable = [
        'brand_id',
        'sort_number',
        'image',
        'same_page',
        'slider_type',
        'slider_type_parent_id',
        'link_type',
        'go_to',
        'status',
    ];


    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
