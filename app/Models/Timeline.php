<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use HasFactory;

    protected $fillable = [
        'year',
        'title',
        'image',
        'logo',
        'description',
        'status'
    ];


    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
