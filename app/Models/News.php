<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class News extends Model
{
    use HasFactory;

    use Sluggable;
    use SluggableScopeHelpers;
    protected $fillable = [
        'brand_id',
        'title',
        'date',
        'image',
        'body',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status',
        'slug',
    ];

    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
