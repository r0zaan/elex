<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->unsignedBigInteger('product_feature_id');
            $table->foreign('product_feature_id')->references('id')->on('product_features')->onDelete('cascade');
            $table->unsignedBigInteger('product_feature_value_id')->nullable();
            $table->foreign('product_feature_value_id')->references('id')->on('product_feature_values')->onDelete('set null');
            $table->string('value');
            $table->integer('sort_number')->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specifications');
    }
}
