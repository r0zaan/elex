<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandPageSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_page_sliders', function (Blueprint $table) {
            $table->id();
            $table->integer('sort_number')->default(1)->nullable();
            $table->string('image');
            $table->boolean('same_page')->default(1);
            $table->enum('slider_type',['Brand','Category','Sub Category', 'Sub Category Type']);
            $table->integer('slider_type_parent_id')->nullable();
            $table->enum('link_type',['Link','Slug','None'])->default('None');
            $table->string('go_to')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_page_sliders');
    }
}
