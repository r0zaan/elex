<?php

use App\Http\Controllers\BrandController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\HomeController@index')->name('home');
Route::get('/admin/login','Backend\LoginController@index')->name('admin.login');
Route::post('/admin/login','Backend\LoginController@login')->name('admin.login.post');

Route::get('/page/{slugString}','Frontend\CustomPageController@index')->name('custom.page');
Route::get('/brand/{slugString}','Frontend\BrandController@index')->name('brand.page');


Route::get('/brand/{slugString}/{productModel}','Frontend\Brand\ProductsController@index')->name('brand.product.detail');



Route::prefix('backend')->middleware('backend')->group(function () {

    Route::get('/dashboard','Backend\DashboardController@index')->name('admin.dashboard');

    Route::resource('/brands', \Backend\BrandController::class);
    Route::resource('/categories', \Backend\CategoryController::class);
    Route::resource('/sub-categories', \Backend\SubCategoryController::class);
    Route::resource('/sub-category-types', \Backend\SubCategoryTypeController::class);
    Route::resource('/brandPageSliders', \Backend\BrandPageSliderController::class);
    Route::resource('/product-features', \Backend\ProductFeatureController::class);
    Route::resource('/product-feature-values', \Backend\ProductFeatureValueController::class);
    Route::resource('/products', \Backend\ProductController::class);
    Route::post('/product-image-delete/{imageId}', 'Backend\ProductController@destroyProductImage');


    Route::get('form/product/ajax/sub-categories-via-category/{categoryId}', function($categoryId){
        $category = \App\Models\Category::find($categoryId);
        return $category->subCategories()->orderBy('name', 'asc')->pluck('name', 'id');
    });
    Route::get('form/product/ajax/sub-category-types-via-sub-category/{subCategoryId}', function($subCategoryId){
        $subCategory = \App\Models\SubCategory::find($subCategoryId);
        return $subCategory->subCategoryTypes()->orderBy('name', 'asc')->pluck('name', 'id');
    });


    Route::get('form/product/ajax/feature_values-via-feature/{featureId}', function($featureId){
        $feature = \App\Models\ProductFeature::find($featureId);
        return $feature->productFeatureValues()->orderBy('value', 'asc')->pluck('value', 'value');
    });


    Route::get('brand/{brandId}/profile', function($brandId){
        $brand = \App\Models\Brand::find($brandId);
        return view('backend.brand.profile', compact('brand'));
    })->name('brand.profile');

    Route::resource('/pages', \Backend\PageController::class);
    Route::resource('/news', \Backend\NewsController::class);
    Route::resource('/timelines', \Backend\TimelineController::class);
    Route::resource('/homeSliders', \Backend\HomeSliderController::class);
    Route::resource('/menuBuilders', \Backend\MenuBuilderController::class);
    Route::get('/menuBuilders/copy/{menuBuilder}', 'Backend\MenuBuilderController@copyMenu')->name('menuBuilders.copy');

    Route::get('/menuBuilders/parent/{parentId}', 'Backend\MenuBuilderController@checkParent')->name('menuBuilders.checkparent');

    Route::resource('/abouts', \Backend\AboutController::class);
    Route::get('/admin/logout','Backend\LoginController@logout')->name('admin.logout');

});
